/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistencia.dao;

import br.com.serverus.models.Coordinates;
import br.com.serverus.models.Itinerary;
import br.com.serverus.models.BusLine;
import br.com.serverus.models.Path;
import br.com.serverus.models.Schedule;
import br.com.serverus.models.ScheduleC;
import br.com.serverus.models.ScheduleType;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe que executa as operações de armazenar os dados em uma base local
 * relacional
 *
 * @author eduardo-lima
 */
@Deprecated
public class SQL {

    private static final String SQL_SERVICE = "jdbc:mysql://localhost:3306/serverus";
    private static final String USER = "root";

    private static final String SAVE_BUS
            = "INSERT INTO ONIBUS (ID, NOME) VALUES (?, ?)";
    private static final String DELETE_BUS
            = "DELETE FROM ONIBUS WHERE ID = ?";
    private static final String GET_ALL_BUS
            = "SELECT * FROM ONIBUS ORDER BY ID";

    private static final String SAVE_ROAD
            = "INSERT INTO RUAS (DESCRICAO) VALUES (?)";
    private static final String DELETE_ROAD
            = "DELETE FROM RUAS WHERE ID = ?";
    private static final String GET_ALL_ROADS
            = "SELECT * FROM RUAS";
    private static final String GET_BUS_BY_ROAD
            = "SELECT O.ID, O.NOME FROM ONIBUS O JOIN RUAS_HAS_ONIBUS R ON (O.ID = R.ONIBUS_ID) WHERE R.RUAS_ID = ?";

    private static final String SAVE_COORDINATES
            = "INSERT INTO COORDENADAS (ONIBUS_ID, IDAVOLTA, LATITUDE, LONGITUDE) VALUES (?, ?, ?, ?)";
    private static final String GET_ALL_COORDINATES
            = "SELECT ID, LATITUDE, LONGITUDE FROM COORDENADAS WHERE ONIBUS_ID = ? AND IDAVOLTA = ?";
    private static final String DELETE_COORDINATES_BY_BUS
            = "DELETE FROM COORDENADAS WHERE ONIBUS_ID = ? AND IDAVOLTA IN (?)";

    private static final String SAVE_ITINERARY
            = "INSERT INTO RUAS_HAS_ONIBUS (RUAS_ID, ONIBUS_ID) VALUES (?, ?)";
    private static final String DELETE_ITINERARY
            = "DELETE FROM RUAS_HAS_ONIBUS WHERE ONIBUS_ID = ?";

    private static final String SAVE_SCHEDULE
            = "INSERT INTO HORARIO (ONIBUS_ID, TIPOHORARIO_ID, SAIDA, IDAVOLTA) VALUES (?, ?, ?, ?)";
    private static final String GET_SCHEDULES_BY_BUS_LINE
            = "SELECT SAIDA FROM HORARIO WHERE ONIBUS_ID = ? AND TIPOHORARIO_ID = ? AND IDAVOLTA = ? ORDER BY SAIDA";
    private static final String GET_ALL_SCHEDULES
            = "SELECT H.ID, H.ONIBUS_ID, O.NOME, H.TIPOHORARIO_ID, T.DESCRICAO, H.SAIDA, H.IDAVOLTA FROM HORARIO H JOIN ONIBUS O ON (H.ONIBUS_ID = O.ID) JOIN TIPOHORARIO T ON (H.TIPOHORARIO_ID = T.ID) ORDER BY H.ONIBUS_ID, H.IDAVOLTA, H.TIPOHORARIO_ID, H.SAIDA";
    private static final String GET_SCHEDULES_BY_BUS 
            = "SELECT H.ID, H.ONIBUS_ID, O.NOME, H.TIPOHORARIO_ID, T.DESCRICAO, H.SAIDA, H.IDAVOLTA FROM HORARIO H JOIN ONIBUS O ON (H.ONIBUS_ID = O.ID) JOIN TIPOHORARIO T ON (H.TIPOHORARIO_ID = T.ID) WHERE H.ONIBUS_ID = ? AND H.TIPOHORARIO_ID = ? AND H.IDAVOLTA = ? ORDER BY H.ONIBUS_ID, H.IDAVOLTA, H.TIPOHORARIO_ID, H.SAIDA";
    private static final String DELETE_SCHEDULE 
            = "DELETE FROM HORARIO WHERE ID = ?";
    
    private static final String GET_ALL_SCHEDULE_TYPES
            = "SELECT ID, DESCRICAO FROM TIPOHORARIO";

    /**
     * Obtém a conexão do banco de dados
     *
     * @return Objeto com as informações de conexão
     * @throws SQLException Exceção SQL
     */
    private static Connection getConnection() throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            throw new SQLException(ex.getMessage());
        }
        return DriverManager.getConnection(SQL_SERVICE, USER, "");
    }

    /**
     * Método que executa o comando SQL para inserir registros de ônibus na base
     *
     * @param o Objeto de ônibus
     * @throws SQLException Execeção SQL
     */
    public static void save(BusLine o) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.SAVE_BUS);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, o.getIdSQL());
            stmt.setString(2, o.getDescription());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método que executa o comando SQL para inserir registros de rua na base
     *
     * @param r Objeto Path
     * @throws SQLException Execeção SQL
     */
    public static void save(Path r) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.SAVE_ROAD);
            stmt.setString(1, r.getDescription());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método que executa o comando SQL para inserir registros de coordenadas do
     * percurso
     *
     * @param c Coordinates do percurso
     * @throws SQLException Execeção SQL
     */
    public static void save(Coordinates c) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.SAVE_COORDINATES);
            stmt.setString(1, c.getBusLineID());
            stmt.setInt(2, c.getIdaVolta());
            stmt.setDouble(3, c.getLatitude());
            stmt.setDouble(4, c.getLongitude());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    public static void save(Itinerary i) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.SAVE_ITINERARY);
            stmt.setString(1, i.getPathId());
            stmt.setString(2, i.getBusLineId());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    public static void save(Schedule s) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.SAVE_SCHEDULE);
            stmt.setString(1, s.getBusId());
            stmt.setInt(2, s.getScheduleType());
            stmt.setInt(3, s.getDepartureTime());
            stmt.setInt(4, s.getRouteType());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    public static void deleteCoordinates(String codigo, boolean... param) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.DELETE_COORDINATES_BY_BUS);
            stmt.setString(1, codigo);

            String idaVolta;
            if ((param[0] == true) && (param[1] == true)) {
                idaVolta = "0,1";
            } else if (param[0] == true) {
                idaVolta = "0";
            } else {
                idaVolta = "1";
            }
            stmt.setString(2, idaVolta);

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }

    }

    /**
     * Método que executa o comando necessário para remover o registro de
     * horário
     *
     * @param id Código do registro
     * @throws SQLException Exceção SQL
     */
    public static void deleteSchedule(int id) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.DELETE_SCHEDULE);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setInt(1, id);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Méotodo que executa o comando necessário para remover o registro de
     * acordo com o código
     *
     * @param codigo Código da Linha do onibus a excluir
     * @throws SQLException Execeção SQL
     */
    public static void deleteBusLine(String codigo) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.DELETE_BUS);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, codigo);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Méotodo que executa o comando necessário para remover o registro de
     * acordo com o código
     *
     * @param codigo Código da rua a excluir
     * @throws SQLException Execeção SQL
     */
    public static void deleteRoad(int codigo) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.DELETE_ROAD);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setInt(1, codigo);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    public static void deleteItinerary(String codigo) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.DELETE_ITINERARY);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, codigo);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método que executa o comando de buscar os horários de ônibus do BD
     *
     * @return Lista com objetos Schedule
     * @throws SQLException Exceção SQL
     */
    public static List<ScheduleC> getAllSchedules() throws SQLException, ParseException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<ScheduleC> lista = new ArrayList();
        try {
            connection = SQL.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL.GET_ALL_SCHEDULES);
            while (resultSet.next()) {
                // Para traduzir o valor em minutos em texto
                String hourS = Integer.toString(resultSet.getInt("saida") / 60);
                String minuteS = Integer.toString(resultSet.getInt("saida") % 60);
                Date d = new SimpleDateFormat("HH:mm").parse(hourS + ":" + minuteS);

                ScheduleC s = new ScheduleC();
                s.setIdS(resultSet.getInt("id"));
                s.setBusId(resultSet.getString("onibus_id"));
                s.setBusDesc(resultSet.getString("nome"));
                s.setScheduleTypeDesc(resultSet.getString("descricao"));
                s.setRouteTypeDesc((resultSet.getInt("idaVolta") == 0 ? "ida" : "volta"));
                s.setDepartureTimeDesc(new SimpleDateFormat("HH:mm").format(d));

                lista.add(s);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    /**
     * Método que executa o comando de buscar os dados do banco de dados SQL
     *
     * @return Lista com objetos de BusLine
     * @throws SQLException Execeção SQL
     */
    public static List<BusLine> getAllBusLine() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<BusLine> lista = new ArrayList();
        try {
            connection = SQL.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL.GET_ALL_BUS);
            while (resultSet.next()) {
                BusLine o = new BusLine();
                o.setIdSQL(resultSet.getString(1));
                o.setDescription(resultSet.getString(2));
                lista.add(o);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    public static List<ScheduleType> getAllScheduleTypes() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<ScheduleType> lista = new ArrayList();
        try {
            connection = SQL.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL.GET_ALL_SCHEDULE_TYPES);
            while (resultSet.next()) {
                ScheduleType t = new ScheduleType();
                t.setIdS(resultSet.getInt(1));
                t.setDescricao(resultSet.getString(2));
                lista.add(t);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    /**
     * Método que executa o comando de buscar os dados do banco de dados SQL
     *
     * @return Lista com objetos de Path
     * @throws SQLException Execeção SQL
     */
    public static List<Path> getAllRoads() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Path> lista = new ArrayList();
        try {
            connection = SQL.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL.GET_ALL_ROADS);
            while (resultSet.next()) {
                Path r = new Path();
                r.setIdSQL(resultSet.getInt(1));
                r.setDescription(resultSet.getString(2));
                lista.add(r);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    public static List<Schedule> getSchedulesByBusLine(String code, int type, int routeType) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<Schedule> lista = new ArrayList<>();
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.GET_SCHEDULES_BY_BUS_LINE);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, code);
            stmt.setInt(2, type);
            stmt.setInt(3, routeType);
            // Executar a operacao de gravar os dados na base
            stmt.executeQuery();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Schedule s = new Schedule();
                s.setBusId(code);
                s.setRouteType(routeType);
                s.setScheduleType(type);
                s.setDepartureTime(resultSet.getInt(1));

                lista.add(s);
            }
        } finally {
            stmt.close();
            conn.close();
        }
        return lista;
    }
    
    public static List<ScheduleC> getSchedulesCByBusLine(String code, int type, int routeType) throws SQLException, ParseException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<ScheduleC> lista = new ArrayList<>();
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.GET_SCHEDULES_BY_BUS);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, code);
            stmt.setInt(2, type);
            stmt.setInt(3, routeType);
            // Executar a operacao de gravar os dados na base
            stmt.executeQuery();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                // Para traduzir o valor em minutos em texto
                String hourS = Integer.toString(resultSet.getInt("saida") / 60);
                String minuteS = Integer.toString(resultSet.getInt("saida") % 60);
                Date d = new SimpleDateFormat("HH:mm").parse(hourS + ":" + minuteS);

                ScheduleC s = new ScheduleC();
                s.setIdS(resultSet.getInt("id"));
                s.setBusId(resultSet.getString("onibus_id"));
                s.setBusDesc(resultSet.getString("nome"));
                s.setScheduleTypeDesc(resultSet.getString("descricao"));
                s.setRouteTypeDesc((resultSet.getInt("idaVolta") == 0 ? "ida" : "volta"));
                s.setDepartureTimeDesc(new SimpleDateFormat("HH:mm").format(d));
                
                lista.add(s);
            }
        } finally {
            stmt.close();
            conn.close();
        }
        return lista;
    }

    public static List<BusLine> getBusLineByRoad(Path r) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<BusLine> lista = new ArrayList<>();
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.GET_BUS_BY_ROAD);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setInt(1, r.getIdSQL());
            // Executar a operacao de gravar os dados na base
            stmt.executeQuery();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                BusLine o = new BusLine();
                o.setIdSQL(resultSet.getString(1));
                o.setDescription(resultSet.getString(2));

                lista.add(o);
            }
        } finally {
            stmt.close();
            conn.close();
        }
        return lista;
    }

    public static List<Coordinates> getCoordinates(String codigo, int idaVolta) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<Coordinates> lista = new ArrayList<>();
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL.GET_ALL_COORDINATES);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, codigo);
            stmt.setInt(2, idaVolta);

            // Executar a operacao de gravar os dados na base
            stmt.executeQuery();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Coordinates c = new Coordinates();
                c.setBusLineID(codigo);
                c.setIdaVolta(idaVolta);
                c.setLatitude(resultSet.getDouble("latitude"));
                c.setLongitude(resultSet.getDouble("longitude"));

                lista.add(c);
            }
        } finally {
            stmt.close();
            conn.close();
        }
        return lista;
    }
}
