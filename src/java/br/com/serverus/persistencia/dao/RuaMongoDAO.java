/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistencia.dao;

import br.com.serverus.models.Path;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;

/**
 * Classe que faz a interação com o banco de dados MongoDB
 *
 * @author home
 */
public class RuaMongoDAO extends BasicDAO<Path, ObjectId> {

// Abaixo é a conexão para o serviço do MongoLAB, funciona!
    private static final String SERVICO_MONGOLAB = "mongodb://admin:admin@ds027751.mongolab.com:27751/serverus";
    private static final String NOME_DATABASE = "serverus";

    private RuaMongoDAO() throws UnknownHostException {
        // Aqui é determinado o servidor e o nome da coleção do meu BD
        super(new MongoClient(new MongoClientURI(SERVICO_MONGOLAB)), new Morphia(), NOME_DATABASE);
    }

    /**
     * Buscar todas as ruas registradas no banco de dados
     *
     * @return Lista de todos os registros ruas
     * @throws java.net.UnknownHostException Exceção lançada pelo morphia
     */
    public static List<Path> buscarTodasRuas() throws UnknownHostException {
        return new RuaMongoDAO().find().asList();
    }

    /**
     * Adicionar o documento de uma nova rua no mongoDB
     *
     * @param r Objeto que será salvo
     * @throws java.net.UnknownHostException Exceção lançada pelo morphia
     */
    public static void adicionarNovaRua(Path r) throws UnknownHostException {

        r.setIdMongo(new ObjectId());
        new RuaMongoDAO().save(r);
    }

    /**
     * Remover o documento da rua do db
     *
     * @param codigo Código da linha de ônibus (não object id)
     * @throws java.net.UnknownHostException Exceção lançada pelo morphia
     */
    public static void removerRua(String codigo) throws UnknownHostException {

        RuaMongoDAO dao = new RuaMongoDAO();

        Query<Path> q = dao.createQuery();
        q.criteria("idStr").containsIgnoreCase(codigo);

        dao.deleteByQuery(q);
    }

}
