package br.com.serverus.persistencia.dao;

import br.com.serverus.models.BusLine;
import br.com.serverus.models.BusRide;
import br.com.serverus.models.BusRideC;
import br.com.serverus.models.Coordinates;
import br.com.serverus.models.Itinerary;
import br.com.serverus.models.Path;
import br.com.serverus.models.Schedule;
import br.com.serverus.models.ScheduleC;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe que executa as operações de armazenar os dados em uma base local
 * relacional
 *
 * @author eduardo-lima
 */
public class SQL2 {

    private static final String SQL_SERVICE = "jdbc:mysql://localhost:3306/serverus4";
    private static final String USER = "root";

    private static final String SAVE_BUS
            = "INSERT INTO BUSLINE (ID, DESCRIPTION) VALUES (?, ?)";
    private static final String GET_ALL_BUS
            = "SELECT * FROM BUSLINE ORDER BY ID";
    private static final String DELETE_BUS
            = "DELETE FROM BUSLINE WHERE ID = ?";
    private static final String GET_BUS_BY_PATH
            = "SELECT B.ID, B.DESCRIPTION FROM BUSLINE B JOIN ITINERARY I ON (B.ID = I.BUSLINE_ID) WHERE I.PATHS_ID = ?";

    private static final String SAVE_PATH
            = "INSERT INTO PATHS (DESCRIPTION) VALUES (?)";
    private static final String DELETE_PATH
            = "DELETE FROM PATHS WHERE ID = ?";
    private static final String GET_ALL_PATH
            = "SELECT * FROM PATHS";

    private static final String SAVE_ITINERARY
            = "INSERT INTO ITINERARY (PATHS_ID, BUSLINE_ID) VALUES (?, ?)";
    private static final String DELETE_ITINERARY
            = "DELETE FROM ITINERARY WHERE BUSLINE_ID = ?";

    private static final String SAVE_SCHEDULE
            = "INSERT INTO BUSSCHEDULE (BUSLINE_ID, BUSSCHEDULETYPE_ID, DEPARTURETIME, ROUTETYPE) VALUES (?, ?, ?, ?)";
    private static final String GET_SCHEDULES_BY_BUS_LINE
            = "SELECT ID, DEPARTURETIME FROM BUSSCHEDULE WHERE BUSLINE_ID = ? AND BUSSCHEDULETYPE_ID = ? AND ROUTETYPE = ? ORDER BY DEPARTURETIME";
    private static final String GET_ALL_SCHEDULES
            = "SELECT S.ID, S.BUSLINE_ID, B.DESCRIPTION BUSDESC, S.BUSSCHEDULETYPE_ID, T.DESCRIPTION SCHEDULEDESC, S.DEPARTURETIME, S.ROUTETYPE FROM BUSSCHEDULE S JOIN BUSLINE B ON (S.BUSLINE_ID = B.ID) JOIN SCHEDULETYPE T ON (S.BUSSCHEDULETYPE_ID = T.ID) ORDER BY S.BUSLINE_ID, S.ROUTETYPE, S.BUSSCHEDULETYPE_ID, S.DEPARTURETIME";
    private static final String DELETE_SCHEDULE
            = "DELETE FROM BUSSCHEDULE WHERE ID = ?";

    private static final String SAVE_BUSRIDE
            = "INSERT INTO BUSRIDE (BUSSCHEDULE_ID, BUSLINE_ID, DURATION, DISTANCE) VALUES (?, ?, ?, ?)";
    private static final String GET_ALL_BUSRIDE
            = "SELECT B.ID BUS_ID, B.DESCRIPTION BUS_DESC, S.ID SCHEDULE_ID, S.ROUTETYPE ROUTE_TYPE, S.DEPARTURETIME DEPARTURE_TIME, T.DESCRIPTION SCHEDULE_TYPE_DESC, R.DURATION DURATION, R.DISTANCE DISTANCE,\n"
            + "CASE WHEN (SELECT COUNT(1) FROM COORDINATES WHERE BUSRIDE_BUSSCHEDULE_ID = R.BUSSCHEDULE_ID AND BUSRIDE_BUSLINE_ID = R.BUSLINE_ID) THEN \"Sim\" ELSE \"Não\" END FLAG\n"
            + "           FROM \n"
            + "              BUSLINE B\n"
            + "               JOIN BUSSCHEDULE S ON (B.ID = S.BUSLINE_ID)\n"
            + "               JOIN SCHEDULETYPE T ON (S.BUSSCHEDULETYPE_ID = T.ID)        JOIN BUSRIDE R ON (B.ID = R.BUSLINE_ID AND S.ID = R.BUSSCHEDULE_ID)\n"
            + "           ORDER BY B.ID, S.ROUTETYPE, T.ID, S.DEPARTURETIME";
    private static final String DELETE_BUSRIDE
            = "DELETE FROM BUSRIDE WHERE BUSLINE_ID = ? AND BUSSCHEDULE_ID = ?";

    private static final String SAVE_COORDINATES
            = "INSERT INTO COORDINATES (BUSRIDE_BUSSCHEDULE_ID, BUSRIDE_BUSLINE_ID, LATITUDE, LONGITUDE) VALUES (?, ?, ?, ?)";
    private static final String DELETE_COORDINATES
            = "DELETE FROM COORDINATES WHERE BUSRIDE_BUSLINE_ID = ? AND BUSRIDE_BUSSCHEDULE_ID = ?";
    private static final String GET__COORDINATES
            = "SELECT ID, LATITUDE, LONGITUDE FROM COORDINATES WHERE BUSRIDE_BUSSCHEDULE_ID = ? AND BUSRIDE_BUSLINE_ID = ?";

    /**
     * Obtém a conexão do banco de dados
     *
     * @return Objeto com as informações de conexão
     * @throws SQLException Exceção SQL
     */
    private static Connection getConnection() throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            throw new SQLException(ex.getMessage());
        }
        return DriverManager.getConnection(SQL_SERVICE, USER, "");
    }

    /**
     * Método que executa o comando SQL para inserir registros de ônibus na base
     *
     * @param b Objeto de ônibus
     * @throws SQLException Execeção SQL
     */
    public static void save(BusLine b) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.SAVE_BUS);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, b.getIdSQL());
            stmt.setString(2, b.getDescription());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Méotodo que executa o comando necessário para remover o registro de
     * acordo com o código
     *
     * @param code Código da Linha do onibus a excluir
     * @throws SQLException Execeção SQL
     */
    public static void deleteBusLine(String code) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.DELETE_BUS);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, code);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Executa o comando para buscar linhas pelo caminho
     *
     * @param p Objeto Path
     * @return Lista de linhas que passam no caminho informado
     * @throws SQLException
     */
    public static List<BusLine> getBusLineByRoad(Path p) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<BusLine> lista = new ArrayList<>();
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.GET_BUS_BY_PATH);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setInt(1, p.getIdSQL());
            // Executar a operacao de gravar os dados na base
            stmt.executeQuery();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                BusLine o = new BusLine();
                o.setIdSQL(resultSet.getString(1));
                o.setDescription(resultSet.getString(2));

                lista.add(o);
            }
        } finally {
            stmt.close();
            conn.close();
        }
        return lista;
    }

    /**
     * Executa o comando SQL para excluir registros de viagens
     *
     * @param busLineID Código da linha
     * @param scheduleID Código do horário
     * @throws SQLException
     */
    public static void deleteBusRide(String busLineID, int scheduleID) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.DELETE_BUSRIDE);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, busLineID);
            stmt.setInt(2, scheduleID);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Executa o comando SQL para excluir registros de viagens
     *
     * @param busLineID Código da linha
     * @param scheduleID Código do horário
     * @throws SQLException
     */
    public static void deleteCoordinates(String busLineID, int scheduleID) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.DELETE_COORDINATES);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, busLineID);
            stmt.setInt(2, scheduleID);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método que executa o comando de buscar os dados do banco de dados SQL
     *
     * @return Lista com objetos de BusLine
     * @throws SQLException Execeção SQL
     */
    public static List<BusLine> getAllBusLine() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<BusLine> lista = new ArrayList();
        try {
            connection = SQL2.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL2.GET_ALL_BUS);
            while (resultSet.next()) {
                BusLine o = new BusLine();
                o.setIdSQL(resultSet.getString(1));
                o.setDescription(resultSet.getString(2));
                lista.add(o);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    /**
     * Método que executa o comando SQL para inserir registros de rua na base
     *
     * @param p Objeto Path
     * @throws SQLException Execeção SQL
     */
    public static void save(Path p) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.SAVE_PATH);
            stmt.setString(1, p.getDescription());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Méotodo que executa o comando necessário para remover o registro de
     * acordo com o código
     *
     * @param code Código da rua a excluir
     * @throws SQLException Execeção SQL
     */
    public static void deletePath(int code) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.DELETE_PATH);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setInt(1, code);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método que executa o comando de buscar os dados do banco de dados SQL
     *
     * @return Lista com objetos de Path
     * @throws SQLException Execeção SQL
     */
    public static List<Path> getAllPath() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Path> lista = new ArrayList();
        try {
            connection = SQL2.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL2.GET_ALL_PATH);
            while (resultSet.next()) {
                Path r = new Path();
                r.setIdSQL(resultSet.getInt(1));
                r.setDescription(resultSet.getString(2));
                lista.add(r);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    /**
     * Método que executa o comando de inserir o itinerário
     *
     * @param i Objeto de itinerário
     * @throws SQLException
     */
    public static void save(Itinerary i) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.SAVE_ITINERARY);
            stmt.setString(1, i.getPathId());
            stmt.setString(2, i.getBusLineId());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método que executa o comando para remover registro de itinerário
     *
     * @param code Código da linha
     * @throws SQLException
     */
    public static void deleteItinerary(String code) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.DELETE_ITINERARY);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, code);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método que executa o comando para inserir horários
     *
     * @param s Objeto de horário
     * @throws SQLException
     */
    public static void save(Schedule s) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.SAVE_SCHEDULE);
            stmt.setString(1, s.getBusId());
            stmt.setInt(2, s.getScheduleType());
            stmt.setInt(3, s.getDepartureTime());
            stmt.setInt(4, s.getRouteType());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método para executar o comando para buscar os horários de uma linha de
     * ônibus
     *
     * @param code Linha do ônibus
     * @param type Tipo de horário
     * @param routeType Tipo de rota
     * @return Lista de horários
     * @throws SQLException Erro de banco
     * @throws java.text.ParseException
     */
    public static List<ScheduleC> getSchedulesByBusLine(String code, int type, int routeType) throws SQLException, ParseException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        ResultSet resultSet;
        List<ScheduleC> lista = new ArrayList<>();
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.GET_SCHEDULES_BY_BUS_LINE);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setString(1, code);
            stmt.setInt(2, type);
            stmt.setInt(3, routeType);
            // Executar a operacao de gravar os dados na base
            stmt.executeQuery();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                // Para traduzir o valor em minutos em texto
                String hourS = Integer.toString(resultSet.getInt("departuretime") / 60);
                String minuteS = Integer.toString(resultSet.getInt("departuretime") % 60);
                Date d = new SimpleDateFormat("HH:mm").parse(hourS + ":" + minuteS);

                ScheduleC s = new ScheduleC();
                s.setDepartureTimeDesc(new SimpleDateFormat("HH:mm").format(d));
                s.setIdS(resultSet.getInt("id"));

                lista.add(s);
            }
        } finally {
            stmt.close();
            conn.close();
        }
        return lista;
    }

    /**
     * Método que executa o comando de buscar os horários de ônibus do BD
     *
     * @return Lista com objetos Schedule
     * @throws SQLException Exceção SQL
     * @throws java.text.ParseException Exceção parser de tempo
     */
    public static List<ScheduleC> getAllSchedules() throws SQLException, ParseException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<ScheduleC> lista = new ArrayList();
        try {
            connection = SQL2.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL2.GET_ALL_SCHEDULES);
            while (resultSet.next()) {
                // Para traduzir o valor em minutos em texto
                String hourS = Integer.toString(resultSet.getInt("departuretime") / 60);
                String minuteS = Integer.toString(resultSet.getInt("departuretime") % 60);
                Date d = new SimpleDateFormat("HH:mm").parse(hourS + ":" + minuteS);

                ScheduleC s = new ScheduleC();
                s.setIdS(resultSet.getInt("id"));
                s.setBusId(resultSet.getString("busline_id"));
                s.setBusDesc(resultSet.getString("busdesc"));
                s.setScheduleTypeDesc(resultSet.getString("scheduledesc"));
                s.setRouteTypeDesc((resultSet.getInt("routetype") == 0 ? "ida" : "volta"));
                s.setDepartureTimeDesc(new SimpleDateFormat("HH:mm").format(d));

                lista.add(s);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    /**
     * Método que executa o comando necessário para remover o registro de
     * horário
     *
     * @param id Código do registro
     * @throws SQLException Exceção SQL
     */
    public static void deleteSchedule(int id) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.DELETE_SCHEDULE);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setInt(1, id);
            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método para executar o comando para salvar as viagens de ônibus
     *
     * @param r Objeto de viagem
     * @throws SQLException
     */
    public static void save(BusRide r) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.SAVE_BUSRIDE);

            stmt.setInt(1, r.getScheduleID());
            stmt.setString(2, r.getBusLineID());
            stmt.setInt(3, r.getDuration());
            stmt.setFloat(4, r.getDistance());

            // Executar a operacao de gravar os dados na base
            stmt.execute();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    /**
     * Método para buscar as viagens de ônibus
     *
     * @return Lista de viagens
     * @throws SQLException
     */
    public static List<BusRideC> getAllBusRide() throws SQLException, ParseException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<BusRideC> lista = new ArrayList();
        try {
            connection = SQL2.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL2.GET_ALL_BUSRIDE);
            while (resultSet.next()) {
                // Para traduzir o valor em minutos em texto
                String hourS = Integer.toString(resultSet.getInt("Departure_Time") / 60);
                String minuteS = Integer.toString(resultSet.getInt("Departure_Time") % 60);
                Date d = new SimpleDateFormat("HH:mm").parse(hourS + ":" + minuteS);

                BusRideC b = new BusRideC();
                b.setBusLineID(resultSet.getString("Bus_id"));
                b.setBusLineDesc(resultSet.getString("Bus_Desc"));
                b.setBusScheduleID(resultSet.getInt("Schedule_id"));
                b.setRouteTypeDesc((resultSet.getInt("Route_Type") == 0 ? "ida" : "volta"));
                b.setDepartureTimeDesc(new SimpleDateFormat("HH:mm").format(d));
                b.setScheduleTypeDesc(resultSet.getString("Schedule_Type_Desc"));
                b.setDurationDesc(resultSet.getString("Duration"));
                b.setDistanceDesc(resultSet.getString("Distance"));
                b.setHasCoordinates(resultSet.getString("flag"));

                lista.add(b);
            }
        } finally {
            connection.close();
            statement.close();
            resultSet.close();
        }
        return lista;
    }

    /**
     * Método para executar o comando para salvar as viagens de ônibus
     *
     * @param c Objeto de coordenadas
     * @throws SQLException
     */
    public static void save(Coordinates c) throws SQLException {
        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.SAVE_COORDINATES);

            stmt.setInt(1, c.getBusScheduleID());
            stmt.setString(2, c.getBusLineID());
            stmt.setDouble(3, c.getLatitude());
            stmt.setDouble(4, c.getLongitude());

            // Executar a operacao de gravar os dados na base
            stmt.executeUpdate();
        } finally {
            stmt.close();
            conn.close();
        }
    }

    public static List<Coordinates> getCoordinates(String busLineID, int scheduleID) throws SQLException {

        // Criar uma variavel para a Conexao
        Connection conn = null;
        // Criar uma variavel para a PreparedStatement
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<Coordinates> lista = new ArrayList<>();
        try {
            // Obtem da ConnectionManager uma conexao com o banco de dados
            conn = SQL2.getConnection();
            // Cria um preparedStatement para o BD conseguir pre-compilar um SQL previamente
            stmt = conn.prepareStatement(SQL2.GET__COORDINATES);
            // Atribui uma String para a 1a. interrogacao (nome)
            stmt.setInt(1, scheduleID);
            stmt.setString(2, busLineID);

            // Executar a operacao de gravar os dados na base
            stmt.executeQuery();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Coordinates c = new Coordinates();
                c.setBusLineID(busLineID);
                c.setBusScheduleID(scheduleID);
                c.setLatitude(resultSet.getDouble("latitude"));
                c.setLongitude(resultSet.getDouble("longitude"));

                lista.add(c);
            }
        } finally {
            stmt.close();
            conn.close();
        }
        return lista;
    }
}
