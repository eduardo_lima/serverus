/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistencia.dao;

import br.com.serverus.models.BusLine;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;

/**
 * Classe que faz a interação com o banco de dados MongoDB
 *
 * @author home
 */
public class OnibusMongoDAO extends BasicDAO<BusLine, ObjectId> {

    // Abaixo é a conexão para o serviço do MongoLAB, funciona!
    private static final String SERVICO_MONGOLAB = "mongodb://admin:admin@ds027751.mongolab.com:27751/serverus";
    private static final String NOME_DATABASE = "serverus";

    private OnibusMongoDAO() throws UnknownHostException {
        // Aqui é determinado o servidor e o nome da coleção do meu BD
        super(new MongoClient(new MongoClientURI(SERVICO_MONGOLAB)), new Morphia(), NOME_DATABASE);
    }

    /**
     * Buscar todas as linhas registradas no banco de dados
     *
     * @return Lista de todos os registros das linhas
     * @throws java.net.UnknownHostException Exceção lançada pelo morphia
     */
    public static List<BusLine> buscarTodasLinhas() throws UnknownHostException {
        return new OnibusMongoDAO().find().asList();
    }

    /**
     * Adicionar o documento de uma nova linha no mongoDB
     *
     * @param o Objeto que será salvo
     * @throws java.net.UnknownHostException Exceção lançada pelo morphia
     */
    public static void adicionarNovaLinha(BusLine o) throws UnknownHostException {

        o.setIdMongo(new ObjectId());
        new OnibusMongoDAO().save(o);
    }

    /**
     * Remover o documento da linha de ônibus do db
     *
     * @param codigo Código da linha de ônibus (não object id)
     * @throws java.net.UnknownHostException Exceção lançada pelo morphia
     */
    public static void removerLinha(String codigo) throws UnknownHostException {

        OnibusMongoDAO dao = new OnibusMongoDAO();

        Query<BusLine> q = dao.createQuery();
        q.criteria("codigo").containsIgnoreCase(codigo);

        dao.deleteByQuery(q);
    }

}
