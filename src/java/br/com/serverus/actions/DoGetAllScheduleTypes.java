/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.ScheduleType;
import br.com.serverus.persistence.SQLScheduleTypes;
import java.util.Calendar;
import java.util.List;

/**
 * Ação responsável em buscar todos os tipos de horários na base
 *
 * @author eduardo-lima
 */
public class DoGetAllScheduleTypes extends ActionBase {

    private List<ScheduleType> list;

    /**
     * Construtor para iniciazliar a ação
     */
    public DoGetAllScheduleTypes() {
        this.init("Obtendo tipos de horário", Calendar.getInstance().getTime(), "anônimo");
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            DatabaseInterface cmd = new SQLScheduleTypes();
            this.list = cmd.getAll();
        } catch (DatabaseEx ex) {
            throw new Exception(ex.getMessage());
        }

        return true;
    }

    /**
     * @return the list
     */
    public List<ScheduleType> getList() {
        return list;
    }

}
