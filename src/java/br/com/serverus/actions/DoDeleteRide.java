/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.ViewRideInterface;
import br.com.serverus.persistence.SQLViewBusRide;
import java.util.Calendar;

/**
 * Ação responsável por remover os registros de viagens
 *
 * @author eduardo-lima
 */
public class DoDeleteRide extends ActionBase {

    // Responsável pela ação
    private final String user;
    // Código da linha
    private final String busLineID;
    // Código do horário
    private final int scheduleID;

    /**
     * Inicializa a ação de exclusão de viagens
     *
     * @param user Responsável pela ação
     * @param busLineID Código da LInha
     * @param scheduleID Código do horário
     */
    public DoDeleteRide(String user, String busLineID, int scheduleID) {
        this.user = user;
        this.busLineID = busLineID;
        this.scheduleID = scheduleID;

        this.init("Removendo registro linha " + this.busLineID + " horário " + Integer.toString(this.scheduleID), Calendar.getInstance().getTime(), this.user);
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            if ((this.busLineID == null) && (this.busLineID.isEmpty())){
                throw new IllegalArgumentException("Código da linha não foi selecionada");
            }
            if (this.scheduleID <= 0){
                throw new IllegalArgumentException("Horário não selecionado");
            }

            ViewRideInterface cmd = new SQLViewBusRide();
            cmd.delete(this.busLineID, this.scheduleID);
        } catch (IllegalArgumentException | DatabaseEx ex) {
            throw new Exception(ex.getMessage());
        };

        return true;
    }

}
