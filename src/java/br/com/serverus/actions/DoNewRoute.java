/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Coordinates;
import br.com.serverus.persistence.SQLCoordinates;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * Ação para inserir os registros de coordenadas do percurso
 *
 * @author eduardo-lima
 */
public class DoNewRoute extends ActionBase {

    // Responsável por add os registros
    private final String user;
    // Arquivo KML que contém as coordenadas
    private final File file;
    // Código da linha para gravar percurso
    private final String busLineID;
    // Código do horário
    private final int busScheduleID;


    /**
     * Construtor para inicializar a inserção de coordenadas de uma viagem
     *
     * @param user Responsável pela ação
     * @param busLineID Código do ônibus
     * @param busScheduleID Código do horário
     * @param file Arquivo de coordenadas
     */
    public DoNewRoute(String user, String busLineID, int busScheduleID, File file) {
        this.user = user;
        this.busLineID = busLineID;
        this.busScheduleID = busScheduleID;
        this.file = file;

        this.init("Adicionando novas coordenadas", Calendar.getInstance().getTime(), this.user);
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            // Fazer validações necessárias
            if ((this.busLineID == null) || (this.busLineID.isEmpty())) {
                throw new IllegalArgumentException("Não foi selecionada a linha de ônibus");
            }
            if (this.busScheduleID <= 0){
                throw new IllegalArgumentException("Não foi selecionado o horário de saída");
            }
            if (this.file == null) {
                throw new IllegalArgumentException("Não foi selecionado arquivo de coordenadas");
            }

            // Obter as informações do documento anexado
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            DocumentBuilder docBuilder;

            docBuilder = dbf.newDocumentBuilder();

            Document doc;
            doc = docBuilder.parse(this.file);

            // Ler as tags do documento
            Element raiz = doc.getDocumentElement();
            Element placemark = (Element) raiz.getElementsByTagName("Placemark").item(0);
            Element lineString = (Element) placemark.getElementsByTagName("LineString").item(0);
            Element coordinates = (Element) lineString.getElementsByTagName("coordinates").item(0);

            // Obtem todos o conteúdo da tag coordinates
            String fullString = coordinates.getTextContent();

            // Cria objeto com informações da coordenada do percurso
            Coordinates coordenada = new Coordinates();
            coordenada.setBusLineID(this.busLineID);
            coordenada.setBusScheduleID(this.busScheduleID);

            // Iterar todo o conteúdo e extrair a latitude e longitude
            while (!fullString.isEmpty()) {
                String longitude = fullString.substring(fullString.indexOf("-"), fullString.indexOf(","));
                fullString = fullString.substring(fullString.indexOf(",") + 1);

                String latitude = fullString.substring(fullString.indexOf("-"), fullString.indexOf(","));
                fullString = fullString.substring(fullString.indexOf(",") + 1);

                coordenada.setLongitude(Double.parseDouble(longitude));
                coordenada.setLatitude(Double.parseDouble(latitude));

                // Comando para inserir as informações no banco de dados
                DatabaseInterface crud = new SQLCoordinates();
                crud.add(coordenada);
            }
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            // Capturar exceções, tratar e relançar para ser capturada no método ActionBase.execute
            throw new Exception("Não foi possível ler o arquivo KML");
        } catch (StringIndexOutOfBoundsException ex) {
            // Exceção silenciada, pois acabou a string do coordinates
        } catch (IllegalArgumentException | DatabaseEx ex) {
            // Validações....
            throw new Exception(ex.getMessage());
        }

        return true;
    }

}
