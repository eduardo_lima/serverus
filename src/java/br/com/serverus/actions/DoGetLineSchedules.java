/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.ScheduleInterface;
import br.com.serverus.models.Schedule;
import br.com.serverus.models.ScheduleC;
import br.com.serverus.persistence.SQLSchedule;
import java.util.Calendar;
import java.util.List;

/**
 * Ação para obter os horários da linha
 *
 * @author eduardo-lima
 */
public class DoGetLineSchedules extends ActionBase {

    // Para buscar o horário é necessário do código da linha
    private final String selectedBusLine;
    // Também é necessário o tipo de horário
    private final int selectedType;
    // Flag de ida ou volta;
    private final int selectedRouteType;
    // Lista dos horários
    private List<ScheduleC> list;

    /**
     * Construtor para inicializar a ação
     *
     * @param code Código da linha
     * @param type Tipo de horário
     * @param routeType Tipo de rota
     */
    public DoGetLineSchedules(String code, int type, int routeType) {
        this.selectedBusLine = code;
        this.selectedType = type;
        this.selectedRouteType = routeType;
        
        this.init("Buscando horários da linha" + this.selectedBusLine, Calendar.getInstance().getTime(), "anônimo");
    }
    
    @Override
    protected boolean internalExecute() throws Exception {
        
        try {
            if ((this.selectedBusLine == null) || (this.selectedBusLine.isEmpty())){
                throw new IllegalArgumentException("Não foi informado o código da linha");
            }
            
            ScheduleInterface cmd = new SQLSchedule();
            this.list = cmd.getSchedulesCByBusLine(this.selectedBusLine, this.selectedType, this.selectedRouteType);
            
        } catch (DatabaseEx | IllegalArgumentException ex) {
            throw new Exception(ex.getMessage());
        }
        return true;
    }

    /**
     * @return the list
     */
    public List<ScheduleC> getList() {
        return list;
    }

    
}
