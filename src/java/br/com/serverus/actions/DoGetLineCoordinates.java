/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.RouteInterface;
import br.com.serverus.models.Coordinates;
import br.com.serverus.persistence.SQLCoordinates;
import java.util.Calendar;
import java.util.List;

/**
 * Ação para get as coordenadas de uma linha de ônibus
 * @author eduardo-lima
 */
public class DoGetLineCoordinates extends ActionBase{

    // Código da linha
    private final String busLineID;
    // Flag do tipo de rota
    private final int scheduleID;
    // Lista de coordenadas da linha
    private List<Coordinates> list;
    
    /**
     * Construtor para inicializar a ação para get as coordenadas de uma linha
     * @param busLineID Código da linha
     * @param scheduleID Flag do tipo de rota
     */
    public DoGetLineCoordinates(String busLineID, int scheduleID){
        this.busLineID = busLineID;
        this.scheduleID = scheduleID;
        
        this.init("Obtendo coordenadas linha " + this.busLineID , Calendar.getInstance().getTime(), "anônimo");
    }
    
    @Override
    protected boolean internalExecute() throws Exception {
       try {
            if ((busLineID == null) && (busLineID.isEmpty())){
                throw new IllegalArgumentException("Linha não selecionada");
            }
            
            RouteInterface cmd = new SQLCoordinates();
            this.list = cmd.getCoordinates(busLineID, scheduleID);
        } catch (DatabaseEx | IllegalArgumentException ex) {
            throw new Exception(ex.getMessage());
        }
        return true;
    }

    /**
     * @return the list
     */
    public List<Coordinates> getList() {
        return list;
    }
    
}
