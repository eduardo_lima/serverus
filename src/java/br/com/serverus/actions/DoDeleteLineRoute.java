/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.persistence.SQLCoordinates;
import java.util.Calendar;

/**
 * Ação responsável para remover o percurso de uma linha de ônibus
 *
 * @author eduardo-lima
 */
public class DoDeleteLineRoute extends ActionBase {

    // Responsável por remover os registros
    private final String login;
    // Código da linha a remover
    private final String codigo;
    
    // Remover coordenadas de ida?
    private final boolean ida;
    // Remover coordenadas de volta?
    private final boolean volta;
    
    /**
     * Construtor para incicializar a ação
     * @param login Responsável pela execução da ação
     * @param codigo Código da linha
     * @param param Coleção contendo a informação de ida e/ou volta
     */
    public DoDeleteLineRoute(String login, String codigo, boolean... param){
        this.login = login;
        this.codigo = codigo;
        this.ida = param[0];
        this.volta = param[1];
        
        this.init("Removendo a coordenadas da linha: " + this.codigo, Calendar.getInstance().getTime(), this.login);
    }

    @Override
    protected boolean internalExecute() throws Exception {
        if (this.codigo == null || this.codigo.isEmpty()) {
            throw new IllegalArgumentException("Não foi selecionado a linha");
        }
    
        DatabaseInterface crud = new SQLCoordinates(this.ida, this.volta);
        crud.delete(this.codigo);
        
        return true;
    }

}
