/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.ViewScheduleInterface;
import br.com.serverus.models.ScheduleC;
import br.com.serverus.persistence.SQLViewSchedule;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

/**
 * Ação responsável por buscar todos os registros de horários de uma linha
 *
 * @author eduardo-lima
 */
public class DoGetSchedulesBusLine extends ActionBase {

    // Lista de horários formatado
    private List<ScheduleC> list;
    private final String busLine;
    private final int routeType;
    private final int scheduleType;

    public DoGetSchedulesBusLine(String code, int route, int schedule) {
        this.busLine = code;
        this.routeType = route;
        this.scheduleType = schedule;

        this.init("Obtendo horários da linha" + code, Calendar.getInstance().getTime(), "anônimo");
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            ViewScheduleInterface cmd = new SQLViewSchedule();

            this.list = cmd.getSchedulesByBusLine(this.busLine, this.scheduleType, this.routeType);
        } catch (DatabaseEx | IllegalArgumentException ex) {
            throw new Exception(ex.getMessage());
        }

        return true;
    }

    /**
     * @return the list
     */
    public List<ScheduleC> getList() {
        return list;
    }
}
