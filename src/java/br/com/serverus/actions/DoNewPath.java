/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Path;
import br.com.serverus.persistence.MongoDBRuas;
import br.com.serverus.persistence.SQLPath;
import java.util.Calendar;

/**
 * Ação responsável por add ruas na base
 *
 * @author eduardo-lima
 */
public class DoNewPath extends ActionBase {

    // Responsável por add os registros
    private final String login;
    // Nome da rua a add
    private final String nome;

    /**
     * Construtor para add novo registro rua
     *
     * @param login Responsável por add a nova rua
     * @param nome Nome da Path
     */
    public DoNewPath(String login, String nome) {
        this.login = login;
        this.nome = nome;

        this.init("Adicionado a rua: " + this.nome, Calendar.getInstance().getTime(), this.login);
    }

    @Override
    protected boolean internalExecute() throws Exception {
        if (this.nome.isEmpty()) {
            throw new IllegalArgumentException("Não foi informado o código o nome da rua");
        }
        
        Path rua = new Path();
        rua.setDescription(this.nome);
        
        DatabaseInterface crud = new SQLPath();
        try {
            crud.add(rua);
        } catch (DatabaseEx ex) {
            throw new Exception(ex.getMessage());
        }

        return true; 
    }

}
