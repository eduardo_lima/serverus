/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Path;
import br.com.serverus.persistence.SQLPath;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Ação responsável por buscar todos os registros das ruas
 *
 * @author eduardo-lima
 */
public class DoGetAllPaths extends ActionBase {

    // Lista que será usada para retorno
    private List<Path> list;

    /**
     * Construtor para inicializar a ação para get as ruas
     */
    public DoGetAllPaths() {
        this.init("Buscando registros de ruas", Calendar.getInstance().getTime(), "anônimo");
    }

    @Override
    protected boolean internalExecute() throws DatabaseEx {
        this.setList(new ArrayList<>());
        
        DatabaseInterface crud = new SQLPath();
        this.getList().addAll(crud.getAll());
        
        return true;
    }

    /**
     * @return the list
     */
    public List<Path> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<Path> list) {
        this.list = list;
    }

}
