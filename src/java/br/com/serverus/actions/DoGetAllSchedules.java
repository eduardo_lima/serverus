/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.ScheduleC;
import br.com.serverus.persistence.SQLViewSchedule;
import java.util.Calendar;
import java.util.List;

/**
 * Ação responsável por buscar todos os registros de horários
 *
 * @author eduardo-lima
 */
public class DoGetAllSchedules extends ActionBase {

    private List<ScheduleC> list;

    /**
     * Construtor para inicializar a ação para get todos os registros de horário
     */
    public DoGetAllSchedules() {
        this.init("Obtendo todos os horários", Calendar.getInstance().getTime(), "anônimo");
    }

    @Override
    protected boolean internalExecute() throws Exception {
        
        DatabaseInterface cmd = new SQLViewSchedule();
        this.list = cmd.getAll();
        
        return true;
    }

    /**
     * @return the list
     */
    public List<ScheduleC> getList() {
        return list;
    }

}
