/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.BusLine;
import br.com.serverus.persistence.MongoDBOnibus;
import br.com.serverus.persistence.SQLBusLine;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Ação responsável por buscar todos os registros das linhas de ônibus
 *
 * @author eduardo-lima
 */
public class DoGetAllLines extends ActionBase {

    // Lista que será usada para retorno
    private List<BusLine> list;

    /**
     * Construtor para inicializar a ação para get todos os registros de linha
     */
    public DoGetAllLines() {

        this.init("Buscando registros", Calendar.getInstance().getTime(), "anônimo");
    }

    @Override
    protected boolean internalExecute() throws DatabaseEx {
        this.list = new ArrayList<>();
        
        DatabaseInterface crud = new SQLBusLine();
        this.list.addAll(crud.getAll());
        
        return true;
    }

    /**
     * @return the list
     */
    public List<BusLine> getList() {
        return list;
    }

    /**
     * Retorna a list de objetos com as linhas de ônibus. Quando a ação ainda
     * não estiver executada o valor será null.
     *
     * @param lista the list to set
     */
    public void setList(List<BusLine> lista) {
        this.list = lista;
    }
}
