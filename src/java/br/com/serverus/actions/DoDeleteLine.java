/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.persistence.SQLBusLine;
import java.util.Calendar;

/**
 * Ação responsável por remover o registro e registrar a ação
 *
 * @author eduardo-lima
 */
public class DoDeleteLine extends ActionBase {

    // Responsável por remover os registros
    private String login;
    // Código da linha a remover
    private String codigo;

    /**
     * Construtor para inicializar a ação para remover as linhas
     * @param login Responsável pela ação
     * @param codigo Código da linha
     */
    public DoDeleteLine(String login, String codigo) {
        this.login = login;
        this.codigo = codigo;

        this.init("Removendo a linha: " + codigo, Calendar.getInstance().getTime(), login);
    }

    @Override
    protected boolean internalExecute() throws DatabaseEx {

        if (this.codigo == null || this.codigo.isEmpty()) {
            throw new IllegalArgumentException("Não foi selecionado o código da linha");
        }

        DatabaseInterface crud = new SQLBusLine();
        crud.delete(this.codigo);

        return true;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
