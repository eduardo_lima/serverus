/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.persistence.SQLItinerary;
import java.util.Calendar;

/**
 * Ação responsável por remover o registro de itinerário
 *
 * @author eduardo-lima
 */
public class DoDeleteLineItinerary extends ActionBase {

    // Responsável por add os registros
    private final String login;
    // Código da linha para remover itinerário
    private final String codigoLinha;

    /**
     * Construtor para inicializar a ação para remover o itinerário da linha
     * @param login Responsável pela execução da ação
     * @param codigo Código da linha
     */
    public DoDeleteLineItinerary(String login, String codigo) {
        this.login = login;
        this.codigoLinha = codigo;

        this.init("Removendo itinerário linha " + this.codigoLinha, Calendar.getInstance().getTime(), this.login);
    }

    @Override
    protected boolean internalExecute() throws Exception {
        try {
            if ((this.codigoLinha == null) || (this.codigoLinha.isEmpty())) {
                throw new IllegalArgumentException("Não foi selecionada a linha de ônibus");
            }

            DatabaseInterface crud = new SQLItinerary();
            crud.delete(this.codigoLinha);
        } catch (IllegalArgumentException | DatabaseEx ex) {
            throw new DatabaseEx(ex.getMessage());
        }
        return true;
    }

}
