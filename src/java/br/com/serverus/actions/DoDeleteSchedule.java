/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.persistence.SQLSchedule;
import java.util.Calendar;

/**
 * Ação responsável por remover os registros de horários
 *
 * @author eduardo-lima
 */
public class DoDeleteSchedule extends ActionBase {

    // Responsável pela ação
    private final String userLogin;
    // Código do horário a excluir
    private final int idSchedule;

    /**
     * Construtor para inicializar a ação de remoção de registro
     *
     * @param u Responsável pela ação
     * @param id Identificação do horário
     */
    public DoDeleteSchedule(String u, int id) {
        this.userLogin = u;
        this.idSchedule = id;

        this.init("Removendo horário " + this.idSchedule, Calendar.getInstance().getTime(), this.userLogin);
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            if (this.idSchedule <= 0) {
                throw new IllegalArgumentException("Horário não selecionado");
            }

            DatabaseInterface cmd = new SQLSchedule();
            cmd.delete(Integer.toString(this.idSchedule));
        } catch (IllegalArgumentException | DatabaseEx ex) {
            throw new Exception(ex.getMessage());
        };

        return true;
    }

}
