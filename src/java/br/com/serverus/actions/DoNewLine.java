/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.BusLine;
import br.com.serverus.persistence.MongoDBOnibus;
import br.com.serverus.persistence.SQLBusLine;
import java.util.Calendar;

/**
 * Ação responsável para add novas linhas de ônibus
 *
 * @author eduardo-lima
 */
public class DoNewLine extends ActionBase {

    // Responsável por add os registros
    private String login;
    // Código da linha a add
    private String codigo;
    // Descrição da linha a add
    private String descricao;

    /**
     * Construtor para add novo registro de linha de ônibus
     *
     * @param login Responsável por add a nova linha
     * @param codigo Código da nova linha
     * @param descricao Descrição da nova linha
     */
    public DoNewLine(String login, String codigo, String descricao) {
        this.login = login;
        this.codigo = codigo;
        this.descricao = descricao;

        this.init("Adicionado a linha: " + codigo, Calendar.getInstance().getTime(), login);
    }

    @Override
    protected boolean internalExecute() throws DatabaseEx {

        if (this.codigo.isEmpty()) {
            throw new IllegalArgumentException("Não foi informado o código da linha");
        }
        if (this.codigo.length() > 5){
            throw new IllegalArgumentException("O código da linha deve estar entre cinco (5) caracteres");
        }
        if (this.descricao.isEmpty()) {
            throw new IllegalArgumentException("Não foi informado a descrição da linha");
        }

        BusLine onibus = new BusLine();
        onibus.setIdSQL(this.codigo);
        onibus.setDescription(this.descricao);

        DatabaseInterface crud = new SQLBusLine();
        crud.add(onibus);

        return true;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
