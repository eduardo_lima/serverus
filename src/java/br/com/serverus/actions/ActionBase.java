/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.ActionExecutionEx;
import java.util.Date;

/**
 * Classe base para diversas ações dentro do sistema em que é necessário o
 * registro da ação. ** Baseado no design pattern: Delegate
 *
 * @author eduardo.lima
 */
public abstract class ActionBase {

    private String nome;
    private Date data;
    private String autor;

    /**
     * Iniciar a ação com o nome da ação, data de execução e responsável
     *
     * @param nome Nome da ação
     * @param data Data de execução
     * @param autor Responsável pela ação
     */
    protected void init(String nome, Date data, String autor) {
        this.nome = nome;
        this.data = data;
        this.autor = autor;
    }

    /**
     * Método executado internamente nas classes filhas
     *
     * @return True se houve execução da ação interna
     * @throws java.lang.Exception Qualquer exceção que ocorra durante a execução da ação
     */
    protected abstract boolean internalExecute() throws Exception;

    /**
     * Método que registra a execução da ação
     */
    private void registerAction() {
        System.out.print(getNome() + " em " + getData().toString() + " por " + getAutor());
    }

    /**
     * Método interno para registrar falha na execução
     *
     * @param e Exceção lançada
     */
    private void registerFail(Exception e) {
        System.out.println("FALHA: " + e.getMessage());
    }

    /**
     * Executar a ação
     *
     * @return True se houve a execução da ação
     * @throws br.com.serverus.exceptions.ActionExecutionEx Exceção na execução da ação
     */
    public boolean execute() throws ActionExecutionEx {
        try {
            if (this.internalExecute()) {
                this.registerAction();

                return true;
            }
        } catch (Exception ex) {
            this.registerFail(ex);
            throw new ActionExecutionEx(ex.getMessage());
        }

        return false;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @return the autor
     */
    public String getAutor() {
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(String autor) {
        this.autor = autor;
    }
}
