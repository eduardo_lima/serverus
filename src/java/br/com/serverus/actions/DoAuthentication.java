/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Ação de autenticação de usuário
 *
 * @author eduardo.lima
 */
public class DoAuthentication extends ActionBase {

    private final String login;
    private final String senha;

    /**
     * Mapa que contém os acessos garantidos
     */
    private static final Map<String, String> mapaAcesso = new HashMap<>();

    /*
     Adicionar usuários que possuem acesso no mapa de acesso
     */
    static {
        mapaAcesso.put("ehclima", "ukitaki123");
    }

    /**
     * Inicia a classe de autenticação
     *
     * @param login Login do usuário
     * @param senha Senha do usuário
     */
    public DoAuthentication(String login, String senha) {
        this.login = login;
        this.senha = senha;

        this.init("Autenticação", Calendar.getInstance().getTime(), login);
    }

    @Override
    protected boolean internalExecute() {

        // Autenticar conforme os usuários do mapa
        if (!(mapaAcesso.containsKey(this.login) && mapaAcesso.get(this.login).equals(this.senha))){
            throw new IllegalArgumentException("Usuário e/ou senha incorretos");
        }
        
        return true;
    }

}
