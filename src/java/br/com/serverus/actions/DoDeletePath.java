/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.persistence.MongoDBRuas;
import br.com.serverus.persistence.SQLPath;
import java.util.Calendar;

/**
 * Ação responsável por remover o registro e registrar a ação
 *
 * @author eduardo-lima
 */
public class DoDeletePath extends ActionBase {

    // Responsável por remover os registros
    private final String login;
    // Código da rua a remover
    private final String codigo;

    /**
     * Construtor para inicializar a ação para remover a rua
     * @param login Responsável pela ação
     * @param id Código da rua
     */
    public DoDeletePath(String login, String id) {
        this.login = login;
        this.codigo = id;

        this.init("Removendo a rua: " + this.codigo, Calendar.getInstance().getTime(), this.login);
    }

    @Override
    protected boolean internalExecute() throws DatabaseEx {
        if (this.codigo == null || this.codigo.isEmpty()) {
            throw new IllegalArgumentException("Não foi selecionado a rua");
        }

        DatabaseInterface crud = new SQLPath();
        crud.delete(this.codigo);
        
        return true;
    }

}
