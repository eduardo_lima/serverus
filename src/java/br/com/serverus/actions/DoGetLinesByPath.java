/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.BusLineInterface;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.BusLine;
import br.com.serverus.models.Path;
import br.com.serverus.persistence.SQLBusLine;
import java.util.Calendar;
import java.util.List;

/**
 * Ação para get quais linhas que possuem em seu itinerário a rua definida
 * @author home
 */
public class DoGetLinesByPath extends ActionBase {

    // Path definida
    private final Path ruaEscolhida;
    // Lista de linhas
    private List<BusLine> list = null;

    /**
     * Construtor para inicializar a ação para get linhas
     * @param rua Objeto com informações da rua
     */
    public DoGetLinesByPath(Path rua) {
        this.ruaEscolhida = rua;
        
        this.init("Buscando linhas da rua" , Calendar.getInstance().getTime(), "anônimo");
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            if (ruaEscolhida == null){
                throw new IllegalArgumentException("Rua não definida");
            }
            
            BusLineInterface busca = new SQLBusLine();
            list = busca.getBusLineByRoad(this.ruaEscolhida);
        } catch (DatabaseEx | IllegalArgumentException ex) {
            throw new Exception(ex.getMessage());
        }
        return true;
    }

    /**
     * @return the list
     */
    public List<BusLine> getList() {
        return list;
    }

}
