/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.BusRide;
import br.com.serverus.persistence.SQLBusRide;
import java.util.Calendar;

/**
 * Ação para incluir nova viagem de ônibus
 *
 * @author eduardo-lima
 */
public class DoNewRide extends ActionBase {

    private final String user;
    private final String busLineID;
    private final int scheduleID;
    private final int duration;
    private final float distance;

    /**
     * Construtor para inicializar a criação de novo registro de viagem
     *
     * @param user Usuário responsável por criar
     * @param busLineID Código do ônibus
     * @param scheduleID Horário de viagem
     * @param duration Duração da viagem
     * @param distance Distância do percurso
     */
    public DoNewRide(String user, String busLineID, int scheduleID, int duration, float distance) {
        this.user = user;
        this.busLineID = busLineID;
        this.scheduleID = scheduleID;
        this.duration = duration;
        this.distance = distance;

        this.init("Adicinonando viagem para a linha " + this.busLineID, Calendar.getInstance().getTime(), this.user);
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            if ((this.busLineID == null) && (this.busLineID.isEmpty())) {
                throw new IllegalArgumentException("Não foi informado o código da linha");
            }
            if (this.scheduleID == 0) {
                throw new IllegalArgumentException("Horário não definido");
            }
            if (this.duration == 0) {
                throw new IllegalArgumentException("Duração não definida");
            }
            if (this.distance == 0){
                throw new IllegalArgumentException("Distância não definida");
            }
            
            BusRide r = new BusRide();
            r.setScheduleID(this.scheduleID);
            r.setBusLineID(this.busLineID);
            r.setDuration(this.duration);
            r.setDistance(this.distance);

            DatabaseInterface cmd = new SQLBusRide();
            cmd.add(r);
        } catch (DatabaseEx | IllegalArgumentException ex) {
            throw new Exception(ex.getMessage());
        }
        return true;
    }

}
