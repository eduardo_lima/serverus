/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Schedule;
import br.com.serverus.persistence.SQLSchedule;
import java.sql.SQLException;
import java.util.Calendar;

/**
 * Ação responsável por adicionar os horários de ônibus
 *
 * @author eduardo-lima
 */
public class DoNewSchedule extends ActionBase {

    // Responsável pela ação
    private String login;
    // Código da linha
    private String busLineCode;
    // Tipo da rota
    private int routeType;
    // Tipo de horário
    private int scheduleType;
    // Horário da saída do ônibus (em minutos)
    private int departureTime;

    /**
     * Construtor para inicializar a ação para inserir horários
     *
     * @param login Responsavel pela açao
     * @param code Código da linha de ônibus
     * @param routeType Tipo de rota, horário de ida ou volta
     * @param scheduleType Tipo de horário
     * @param time Tempo da saída do ônibus (em minutos do dia)
     */
    public DoNewSchedule(String login, String code, int routeType, int scheduleType, int time) {
        this.login = login;
        this.busLineCode = code;
        this.routeType = routeType;
        this.scheduleType = scheduleType;
        this.departureTime = time;

        this.init("Incluindo horário para a linha" + this.busLineCode, Calendar.getInstance().getTime(), this.login);
    }

    @Override
    protected boolean internalExecute() throws Exception {

        try {
            if ((this.busLineCode == null) || (this.busLineCode.isEmpty())) {
                throw new IllegalArgumentException("Não informado o código da linha");
            }

            Schedule sd = new Schedule();
            sd.setBusId(this.busLineCode);
            sd.setDepartureTime(this.departureTime);
            sd.setRouteType(this.routeType);
            sd.setScheduleType(this.scheduleType);
            
            DatabaseInterface cmd = new SQLSchedule();
            cmd.add(sd);

        } catch (IllegalArgumentException | DatabaseEx ex) {
            throw new Exception(ex.getMessage());
        }
        return true;
    }

}
