/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.BusRideC;
import br.com.serverus.persistence.SQLViewBusRide;
import java.util.Calendar;
import java.util.List;

/**
 * Ação responsável por buscar as viagens de todos os ônibus
 *
 * @author eduardo-lima
 */
public class DoGetAllBusRides extends ActionBase {

    private List<BusRideC> list;

    /**
     * Construtor para inicializar a ação para get todos os registros de viagens
     */
    public DoGetAllBusRides() {
        this.init("Obtendo todas as viagens", Calendar.getInstance().getTime(), "anônimo");
    }

    @Override
    protected boolean internalExecute() throws Exception {

        DatabaseInterface cmd = new SQLViewBusRide();
        this.list = cmd.getAll();

        return true;

    }

    /**
     * @return the list
     */
    public List<BusRideC> getList() {
        return list;
    }
}
