/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.actions;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Itinerary;
import br.com.serverus.persistence.SQLItinerary;
import java.util.Calendar;

/**
 * Ação que insere registros de itinerário da linha
 *
 * @author eduardo lima
 */
public class DoNewItinerary extends ActionBase {

    // Responsável por add os registros
    private final String login;
    // Código da linha para gravar itinerário
    private final String codigoLinha;

    // Array com as ruas que foram selecionadas
    private final String[] ruasSelecionadas;

    /**
     * Construtor para inicializar a ação para registro de itinerário
     * @param login Responsável pela ação
     * @param codigo Código da linha
     * @param ruasSelecionadas Array com as ruas escolhidas
     */
    public DoNewItinerary(String login, String codigo, String[] ruasSelecionadas) {
        this.login = login;
        this.codigoLinha = codigo;
        this.ruasSelecionadas = ruasSelecionadas;

        this.init("Adicionando novo itinerário", Calendar.getInstance().getTime(), this.login);
    }

    @Override
    protected boolean internalExecute() throws DatabaseEx {

        try {
            // Fazer validações necessárias...
            if ((this.codigoLinha == null) || (this.codigoLinha.isEmpty())) {
                throw new IllegalArgumentException("Não foi selecionada a linha de ônibus");
            }
            if (this.ruasSelecionadas.length <= 0){
                throw new IllegalArgumentException("Sem ruas selecionadas");
            }

            Itinerary itinerario = new Itinerary();
            itinerario.setBusLineId(this.codigoLinha);

            // Salvar no banco de dados
            DatabaseInterface crud = new SQLItinerary();
            for (String ruaId : this.ruasSelecionadas) {
                itinerario.setPathId(ruaId);

                crud.add(itinerario);
            }
        } catch (IllegalArgumentException | DatabaseEx ex) {
            throw new DatabaseEx(ex.getMessage());
        }
        return true;
    }

}
