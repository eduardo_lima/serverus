/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

import javax.xml.bind.annotation.XmlRootElement;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Classe modelo para objetos que representam as ruas
 *
 * @author eduardo-lima
 */
@XmlRootElement
@Entity
public class Path {

    @Id
    private ObjectId idMongo;
    private String idStr;

    private int idSQL;
    private String description;

    /**
     * @return the idMongo
     */
    public ObjectId getIdMongo() {
        return idMongo;
    }

    /**
     * @param idMongo the idMongo to set
     */
    public void setIdMongo(ObjectId idMongo) {
        this.idMongo = idMongo;
        this.idStr = idMongo.toString();
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the idStr
     */
    public String getIdStr() {
        return idStr;
    }

    /**
     * @param idStr the idStr to set
     */
    public void setIdStr(String idStr) {
        this.idStr = idStr;
        this.idMongo = new ObjectId(idStr); // Converter a string para objectID, isso será util nas rotinas executadas no EventoDAO
    }

    /**
     * @return the idSQL
     */
    public int getIdSQL() {
        return idSQL;
    }

    /**
     * @param codigo the idSQL to set
     */
    public void setIdSQL(int codigo) {
        this.idSQL = codigo;
    }
}
