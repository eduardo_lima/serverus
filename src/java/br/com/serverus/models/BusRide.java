/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

import javax.xml.bind.annotation.XmlRootElement;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Classe modelo que representa a viagem dos ônibus
 * @author eduardo-lima
 */
@XmlRootElement
@Entity
public class BusRide {
    
    @Id
    private ObjectId idMongo;
    
    private int idSQL;
    
    private int scheduleID;
    
    private String busLineID;
    
    private int duration;
    
    private float distance;

    /**
     * @return the idMongo
     */
    public ObjectId getIdMongo() {
        return idMongo;
    }

    /**
     * @param idMongo the idMongo to set
     */
    public void setIdMongo(ObjectId idMongo) {
        this.idMongo = idMongo;
    }

    /**
     * @return the idSQL
     */
    public int getIdSQL() {
        return idSQL;
    }

    /**
     * @param idSQL the idSQL to set
     */
    public void setIdSQL(int idSQL) {
        this.idSQL = idSQL;
    }

    /**
     * @return the scheduleID
     */
    public int getScheduleID() {
        return scheduleID;
    }

    /**
     * @param scheduleID the scheduleID to set
     */
    public void setScheduleID(int scheduleID) {
        this.scheduleID = scheduleID;
    }

    /**
     * @return the busLineID
     */
    public String getBusLineID() {
        return busLineID;
    }

    /**
     * @param busLineID the busLineID to set
     */
    public void setBusLineID(String busLineID) {
        this.busLineID = busLineID;
    }

    /**
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * @return the distance
     */
    public float getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(float distance) {
        this.distance = distance;
    }
    
}
