/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

import javax.xml.bind.annotation.XmlRootElement;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Classe que contêm as informações do ônibus
 *
 * @author eduardo-lima
 */
@XmlRootElement
@Entity
public class BusLine {

    @Id
    private ObjectId idMongo;

    private String idSQL;
    private String description;

    /**
     * @return the idSQL
     */
    public String getIdSQL() {
        return idSQL;
    }

    /**
     * @param idSQL the idSQL to set
     */
    public void setIdSQL(String idSQL) {
        this.idSQL = idSQL;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the idMongo
     */
    public ObjectId getIdMongo() {
        return idMongo;
    }

    /**
     * @param idMongo the idMongo to set
     */
    public void setIdMongo(ObjectId idMongo) {
        this.idMongo = idMongo;
    }

}
