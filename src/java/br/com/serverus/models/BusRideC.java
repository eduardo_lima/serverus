/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

/**
 * Classe modelo para consultas de viagens
 * 
 * @author eduardo-lima
 */
public class BusRideC {
    
    private String busLineID;
    
    private String busLineDesc;
    
    private int busScheduleID;
    
    private String departureTimeDesc;
    
    private String routeTypeDesc;
    
    private String scheduleTypeDesc;
    
    private String durationDesc;
    
    private String distanceDesc;
    
    private String hasCoordinates;
    
    /**
     * @return the busLineID
     */
    public String getBusLineID() {
        return busLineID;
    }

    /**
     * @param busLineID the busLineID to set
     */
    public void setBusLineID(String busLineID) {
        this.busLineID = busLineID;
    }

    /**
     * @return the busLineDesc
     */
    public String getBusLineDesc() {
        return busLineDesc;
    }

    /**
     * @param busLineDesc the busLineDesc to set
     */
    public void setBusLineDesc(String busLineDesc) {
        this.busLineDesc = busLineDesc;
    }

    /**
     * @return the busScheduleID
     */
    public int getBusScheduleID() {
        return busScheduleID;
    }

    /**
     * @param busScheduleID the busScheduleID to set
     */
    public void setBusScheduleID(int busScheduleID) {
        this.busScheduleID = busScheduleID;
    }

    /**
     * @return the departureTimeDesc
     */
    public String getDepartureTimeDesc() {
        return departureTimeDesc;
    }

    /**
     * @param departureTimeDesc the departureTimeDesc to set
     */
    public void setDepartureTimeDesc(String departureTimeDesc) {
        this.departureTimeDesc = departureTimeDesc;
    }

    /**
     * @return the routeTypeDesc
     */
    public String getRouteTypeDesc() {
        return routeTypeDesc;
    }

    /**
     * @param routeTypeDesc the routeTypeDesc to set
     */
    public void setRouteTypeDesc(String routeTypeDesc) {
        this.routeTypeDesc = routeTypeDesc;
    }

    /**
     * @return the scheduleTypeDesc
     */
    public String getScheduleTypeDesc() {
        return scheduleTypeDesc;
    }

    /**
     * @param scheduleTypeDesc the scheduleTypeDesc to set
     */
    public void setScheduleTypeDesc(String scheduleTypeDesc) {
        this.scheduleTypeDesc = scheduleTypeDesc;
    }

    /**
     * @return the durationDesc
     */
    public String getDurationDesc() {
        return durationDesc;
    }

    /**
     * @param durationDesc the durationDesc to set
     */
    public void setDurationDesc(String durationDesc) {
        this.durationDesc = durationDesc;
    }

    /**
     * @return the distanceDesc
     */
    public String getDistanceDesc() {
        return distanceDesc;
    }

    /**
     * @param distanceDesc the distanceDesc to set
     */
    public void setDistanceDesc(String distanceDesc) {
        this.distanceDesc = distanceDesc;
    }

    /**
     * @return the hasCoordinates
     */
    public String getHasCoordinates() {
        return hasCoordinates;
    }

    /**
     * @param hasCoordinates the hasCoordinates to set
     */
    public void setHasCoordinates(String hasCoordinates) {
        this.hasCoordinates = hasCoordinates;
    }

}
