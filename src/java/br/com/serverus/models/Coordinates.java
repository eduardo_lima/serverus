/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

import javax.xml.bind.annotation.XmlRootElement;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Classe que mantém a informação de geolocalização de um ponto do percurso
 *
 * @author eduardo-lima
 */
@XmlRootElement
@Entity
public class Coordinates {

    @Id
    private ObjectId idMongo;

    @Deprecated
    private int busRideID;

    private int idSQL;
    private String busLineID;
    private int busScheduleID;

    @Deprecated
    private int idaVolta;

    private double latitude;
    private double longitude;

    /**
     * @return the idMongo
     */
    public ObjectId getIdMongo() {
        return idMongo;
    }

    /**
     * @param idMongo the idMongo to set
     */
    public void setIdMongo(ObjectId idMongo) {
        this.idMongo = idMongo;
    }

    /**
     * @return the busLineID
     */
    public String getBusLineID() {
        return busLineID;
    }

    /**
     * @param busLineID the busLineID to set
     */
    public void setBusLineID(String busLineID) {
        this.busLineID = busLineID;
    }

    /**
     * @return the idaVolta
     */
    public int getIdaVolta() {
        return idaVolta;
    }

    /**
     * @param idaVolta the idaVolta to set
     */
    public void setIdaVolta(int idaVolta) {
        this.idaVolta = idaVolta;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the idSQL
     */
    public int getIdSQL() {
        return idSQL;
    }

    /**
     * @param idSQL the idSQL to set
     */
    public void setIdSQL(int idSQL) {
        this.idSQL = idSQL;
    }

    /**
     * @return the busRideID
     */
    public int getBusRideID() {
        return busRideID;
    }

    /**
     * @param busRideID the busRideID to set
     */
    public void setBusRideID(int busRideID) {
        this.busRideID = busRideID;
    }

    /**
     * @return the busScheduleID
     */
    public int getBusScheduleID() {
        return busScheduleID;
    }

    /**
     * @param busScheduleID the busScheduleID to set
     */
    public void setBusScheduleID(int busScheduleID) {
        this.busScheduleID = busScheduleID;
    }
}
