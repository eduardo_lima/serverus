/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

import javax.xml.bind.annotation.XmlRootElement;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Classe modelo específico para consultas de horários
 * @author eduardo-lima
 */
@XmlRootElement
@Entity
public class ScheduleC {
    
    @Id
    private ObjectId id;
    
    private int idS;
    
    private String busId;
    
    private String busDesc;
    
    private String scheduleTypeDesc;
    
    private String routeTypeDesc;
    
    private String departureTimeDesc;

    /**
     * @return the id
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * @return the idS
     */
    public int getIdS() {
        return idS;
    }

    /**
     * @param idS the idS to set
     */
    public void setIdS(int idS) {
        this.idS = idS;
    }

    /**
     * @return the busId
     */
    public String getBusId() {
        return busId;
    }

    /**
     * @param busId the busId to set
     */
    public void setBusId(String busId) {
        this.busId = busId;
    }

    /**
     * @return the scheduleTypeDesc
     */
    public String getScheduleTypeDesc() {
        return scheduleTypeDesc;
    }

    /**
     * @param scheduleTypeDesc the scheduleTypeDesc to set
     */
    public void setScheduleTypeDesc(String scheduleTypeDesc) {
        this.scheduleTypeDesc = scheduleTypeDesc;
    }

    /**
     * @return the routeTypeDesc
     */
    public String getRouteTypeDesc() {
        return routeTypeDesc;
    }

    /**
     * @param routeTypeDesc the routeTypeDesc to set
     */
    public void setRouteTypeDesc(String routeTypeDesc) {
        this.routeTypeDesc = routeTypeDesc;
    }

    /**
     * @return the departureTimeDesc
     */
    public String getDepartureTimeDesc() {
        return departureTimeDesc;
    }

    /**
     * @param departureTimeDesc the departureTimeDesc to set
     */
    public void setDepartureTimeDesc(String departureTimeDesc) {
        this.departureTimeDesc = departureTimeDesc;
    }

    /**
     * @return the busDesc
     */
    public String getBusDesc() {
        return busDesc;
    }

    /**
     * @param busDesc the busDesc to set
     */
    public void setBusDesc(String busDesc) {
        this.busDesc = busDesc;
    }
}
