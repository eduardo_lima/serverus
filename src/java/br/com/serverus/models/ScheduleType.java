/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

import javax.xml.bind.annotation.XmlRootElement;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Classe modelo que representa o tipo de horário 
 * @author eduardo-lima
 */
@XmlRootElement
@Entity
public class ScheduleType {
    
    @Id
    private ObjectId id;
    
    private int idS;
    
    private String descricao;

    /**
     * @return the id
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * @return the idS
     */
    public int getIdS() {
        return idS;
    }

    /**
     * @param idS the idS to set
     */
    public void setIdS(int idS) {
        this.idS = idS;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
