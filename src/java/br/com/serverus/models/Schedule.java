/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

import javax.xml.bind.annotation.XmlRootElement;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Classe modelo que representa o horário de ônibus
 * @author eduardo-lima
 */
@XmlRootElement
@Entity
public class Schedule {
    
    @Id
    private ObjectId idMongo;
    
    private int idS;
    
    private String busId;
    
    private int scheduleType;
    
    private int routeType;
    
    private int departureTime;

    /**
     * @return the idMongo
     */
    public ObjectId getIdMongo() {
        return idMongo;
    }

    /**
     * @param idMongo the idMongo to set
     */
    public void setIdMongo(ObjectId idMongo) {
        this.idMongo = idMongo;
    }

    /**
     * @return the idS
     */
    public int getIdS() {
        return idS;
    }

    /**
     * @param idS the idS to set
     */
    public void setIdS(int idS) {
        this.idS = idS;
    }

    /**
     * @return the busId
     */
    public String getBusId() {
        return busId;
    }

    /**
     * @param busId the busId to set
     */
    public void setBusId(String busId) {
        this.busId = busId;
    }

    /**
     * @return the scheduleType
     */
    public int getScheduleType() {
        return scheduleType;
    }

    /**
     * @param scheduleType the scheduleType to set
     */
    public void setScheduleType(int scheduleType) {
        this.scheduleType = scheduleType;
    }

    /**
     * @return the routeType
     */
    public int getRouteType() {
        return routeType;
    }

    /**
     * @param routeType the routeType to set
     */
    public void setRouteType(int routeType) {
        this.routeType = routeType;
    }

    /**
     * @return the departureTime
     */
    public int getDepartureTime() {
        return departureTime;
    }

    /**
     * @param departureTime the departureTime to set
     */
    public void setDepartureTime(int departureTime) {
        this.departureTime = departureTime;
    }
    
    
}
