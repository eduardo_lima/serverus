/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.models;

/**
 * Classe que contém as informações de ligação entre a linha de ônibus e a rua que ele passa
 * @author eduardo-lima
 */
public class Itinerary {
    
    private String pathId;
    private String busLineId;

    /**
     * @return the pathId
     */
    public String getPathId() {
        return pathId;
    }

    /**
     * @param pathId the pathId to set
     */
    public void setPathId(String pathId) {
        this.pathId = pathId;
    }

    /**
     * @return the busLineId
     */
    public String getBusLineId() {
        return busLineId;
    }

    /**
     * @param busLineId the busLineId to set
     */
    public void setBusLineId(String busLineId) {
        this.busLineId = busLineId;
    }
}
