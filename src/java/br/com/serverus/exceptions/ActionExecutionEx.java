/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.exceptions;

/**
 * Exceção específica parar as exceções dentro das ações na exceução
 * @author eduardo-lima
 */
public class ActionExecutionEx extends Exception{
    
    public ActionExecutionEx(String msg){
        super(msg);
    }
}
