/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.exceptions;

/**
 * Exceção específica para erros de banco de dados
 * @author eduardo-lima
 */
public class DatabaseEx extends Exception{

    public DatabaseEx(String msg){
        super(msg);
    }
}
