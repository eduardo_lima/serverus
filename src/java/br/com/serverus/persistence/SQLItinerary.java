/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Itinerary;
import br.com.serverus.persistencia.dao.SQL;
import br.com.serverus.persistencia.dao.SQL2;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que contém as operações de banco de dados para com o itinerário
 *
 * @author eduardo-lima
 */
public class SQLItinerary implements DatabaseInterface<Itinerary> {

    @Override
    public void add(Itinerary object) throws DatabaseEx {
        try {
            SQL2.save(object);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            throw new DatabaseEx("Itinerário já definido");
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public void edit(Itinerary object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        try {
            SQL2.deleteItinerary(id);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public Itinerary get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Itinerary> getAll() throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
