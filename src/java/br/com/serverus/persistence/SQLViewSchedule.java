/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.interfaces.ViewScheduleInterface;
import br.com.serverus.models.ScheduleC;
import br.com.serverus.persistencia.dao.SQL;
import br.com.serverus.persistencia.dao.SQL2;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que contém as operações de banco de dados para horários de ônibus
 *
 * @author eduardo-lima
 */
public class SQLViewSchedule implements DatabaseInterface<ScheduleC>, ViewScheduleInterface {

    @Override
    public void add(ScheduleC object) throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(ScheduleC object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ScheduleC get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ScheduleC> getAll() throws DatabaseEx {
        try {
            return SQL2.getAllSchedules();
        } catch (SQLException | ParseException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public List<ScheduleC> getSchedulesByBusLine(String code, int type, int routeType) throws DatabaseEx {
        try {
            return SQL2.getSchedulesByBusLine(code, type, routeType);
        } catch (SQLException | ParseException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

}
