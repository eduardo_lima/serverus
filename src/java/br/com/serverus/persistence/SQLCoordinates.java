/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.RouteInterface;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Coordinates;
import br.com.serverus.persistencia.dao.SQL;
import br.com.serverus.persistencia.dao.SQL2;
import java.sql.SQLException;
import java.util.List;

/**
 * Classe que realiza as operações de banco de dados, especificamente da tabela
 * coordenadas
 *
 * @author eduardo-lima
 */
public class SQLCoordinates implements DatabaseInterface<Coordinates>, RouteInterface {

    private boolean ida;
    private boolean volta;

    /**
     * Construtor que define a informação de percurso de ida e/ou volta. SOMENTE
     * PARA EXCLUSÃO
     *
     * @param param Varargs param[0]: ida - param[1]: volta
     */
    @Deprecated
    public SQLCoordinates(boolean... param) {
        this.ida = param[0];
        this.volta = param[1];
    }

    /**
     * Construtor para a inserção de coordenadas. Na inserção tal informação já
     * está armazenada no objeto Coordinates.
     */
    public SQLCoordinates() {
    }

    @Override
    public void add(Coordinates object) throws DatabaseEx {
        try {
            SQL2.save(object);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public void edit(Coordinates object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        try {
            SQL.deleteCoordinates(id, this.ida, this.volta);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public Coordinates get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Coordinates> getAll() throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Coordinates> getCoordinatesByBusLine(String codigo, int idaVolta) throws DatabaseEx {
        try {
            return SQL.getCoordinates(codigo, idaVolta);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public List<Coordinates> getCoordinates(String busLineID, int scheduleID) throws DatabaseEx {
        try {
            return SQL2.getCoordinates(busLineID, scheduleID);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

}
