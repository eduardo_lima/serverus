/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Path;
import br.com.serverus.persistencia.dao.SQL;
import br.com.serverus.persistencia.dao.SQL2;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author home
 */
public class SQLPath implements DatabaseInterface<Path> {

    @Override
    public void add(Path object) throws DatabaseEx {
        try {
            SQL2.save(object);
        } catch (MySQLIntegrityConstraintViolationException ex) {
            throw new DatabaseEx("Código já foi utilizado");
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public void edit(Path object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        try {
            SQL2.deletePath(Integer.parseInt(id));
        } catch (MySQLIntegrityConstraintViolationException ex) {
            throw new DatabaseEx("Não é possível realizar a exclusão. Tente excluir os registros dependentes.");
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public Path get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Path> getAll() throws DatabaseEx {
        try {
            return SQL2.getAllPath();
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }
    
}
