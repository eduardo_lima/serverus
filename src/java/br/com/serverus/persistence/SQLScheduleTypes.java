/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.ScheduleType;
import br.com.serverus.persistencia.dao.SQL;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que contém as operações de banco de dados dos tipos de horários
 * @author eduardo-lima
 */
public class SQLScheduleTypes implements DatabaseInterface<ScheduleType> {

    @Override
    public void add(ScheduleType object) throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(ScheduleType object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ScheduleType get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ScheduleType> getAll() throws DatabaseEx {
        try {
            return SQL.getAllScheduleTypes();
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }
    
}
