/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.interfaces.ScheduleInterface;
import br.com.serverus.models.Schedule;
import br.com.serverus.models.ScheduleC;
import br.com.serverus.persistencia.dao.SQL;
import br.com.serverus.persistencia.dao.SQL2;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que contém as operações de banco de dados para horários de ônibus
 *
 * @author eduardo-lima
 */
public class SQLSchedule implements DatabaseInterface<Schedule>, ScheduleInterface {

    @Override
    public void add(Schedule object) throws DatabaseEx {
        try {
            SQL2.save(object);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public void edit(Schedule object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        try {
            SQL2.deleteSchedule(Integer.parseInt(id));
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public Schedule get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Schedule> getAll() throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Schedule> getSchedulesByBusLine(String code, int type, int routeType) throws DatabaseEx {
        try {
            return SQL.getSchedulesByBusLine(code, type, routeType);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public List<ScheduleC> getSchedulesCByBusLine(String code, int type, int routeType) throws DatabaseEx {
        try {
            return SQL2.getSchedulesByBusLine(code, type, routeType);
        } catch (SQLException | ParseException ex) {
            throw new DatabaseEx(ex.getMessage());
        }

    }

}
