/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.interfaces.ViewRideInterface;
import br.com.serverus.models.BusRideC;
import br.com.serverus.persistencia.dao.SQL2;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


/**
 * Classe que contém as operações de banco de dados para viagens de ônibus
 *
 * @author eduardo-lima
 */
public class SQLViewBusRide implements DatabaseInterface<BusRideC>, ViewRideInterface{

    @Override
    public void add(BusRideC object) throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(BusRideC object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BusRideC get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BusRideC> getAll() throws DatabaseEx {
        try {
            return SQL2.getAllBusRide();
        } catch (SQLException | ParseException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public void delete(String busLineID, int scheduleID) throws DatabaseEx {
        try {
            SQL2.deleteCoordinates(busLineID, scheduleID);
            SQL2.deleteBusRide(busLineID, scheduleID);
        } catch (SQLException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    
}
