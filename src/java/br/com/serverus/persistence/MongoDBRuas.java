/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.Path;
import br.com.serverus.persistencia.dao.RuaMongoDAO;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Classe que faz as operações de armazenamento dos dados das ruas
 *
 * @author eduardo-lima
 */
public class MongoDBRuas implements DatabaseInterface<Path> {

    @Override
    public void add(Path object) throws DatabaseEx {
        try {
            RuaMongoDAO.adicionarNovaRua(object);
        } catch (UnknownHostException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public void edit(Path object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        try {
            RuaMongoDAO.removerRua(id);
        } catch (UnknownHostException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public Path get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Path> getAll() throws DatabaseEx {
        try {
            return RuaMongoDAO.buscarTodasRuas();
        } catch (UnknownHostException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

}
