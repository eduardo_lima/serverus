/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.persistence;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.interfaces.DatabaseInterface;
import br.com.serverus.models.BusLine;
import br.com.serverus.persistencia.dao.OnibusMongoDAO;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Classe que faz as operações de armazenamento dos dados das linhas de ônibus
 *
 * @author eduardo-lima
 */
public class MongoDBOnibus implements DatabaseInterface<BusLine> {

    @Override
    public void add(BusLine object) throws DatabaseEx {
        try {
            OnibusMongoDAO.adicionarNovaLinha(object);
        } catch (UnknownHostException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public void edit(BusLine object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) throws DatabaseEx {
        try {
            OnibusMongoDAO.removerLinha(id);
        } catch (UnknownHostException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

    @Override
    public BusLine get(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BusLine> getAll() throws DatabaseEx {
        try {
            return OnibusMongoDAO.buscarTodasLinhas();
        } catch (UnknownHostException ex) {
            throw new DatabaseEx(ex.getMessage());
        }
    }

}
