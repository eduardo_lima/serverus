/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans;

import br.com.serverus.actions.DoNewLine;
import br.com.serverus.actions.DoGetAllLines;
import br.com.serverus.actions.DoDeleteLine;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.BusLine;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

/**
 * Gerencia as informações relacionadas as linhas de ônibus
 *
 * @author eduardo-lima
 */
@ManagedBean
public class BusLineMB {
    
    private String codigo;
    private String descricao;
    
    private List<BusLine> lista;
    
    @PostConstruct
    public void init() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        
        DoGetAllLines acao = new DoGetAllLines();
        lista = new ArrayList<>();
        
        try {
            acao.execute();
            
            lista.addAll(acao.getList());
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }
    
    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * informação
     *
     * @param info Informação a ser exibida
     */
    private void addInfo(String info) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, info, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * erro
     *
     * @param erro Erro a ser exibido
     */
    private void addError(String erro) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Método para adicionar linhas na base
     *
     * @param evt Evento de componente jsf
     */
    public void add(ActionEvent evt) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);
        
        DoNewLine acao = new DoNewLine(session.getAttribute("usuario").toString(), codigo, descricao);
        
        try {
            acao.execute(); // Executa a ação
            this.addInfo("Registro adicionado");
            this.codigo = "";
            this.descricao = "";
            
            this.init();
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método para ser executado como multipagina AINDA EM CONSTRUCAO
     *
     * @param evt
     */
    public void delete(ActionEvent evt) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);
        
        DoDeleteLine acao = new DoDeleteLine(session.getAttribute("usuario").toString(), this.codigo);
        
        try {
            acao.execute();
            this.addInfo("Registro removido");
            
            this.codigo = "";
            this.descricao = "";
            
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método para remover a linha selecionada
     *
     * @return Redirecionamento para outra página
     */
    public String delete() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);
        
        DoDeleteLine acao = new DoDeleteLine(session.getAttribute("usuario").toString(), this.codigo);
        
        try {
            acao.execute();
            this.addInfo("Registro removido");
            
            this.codigo = null;
            this.descricao = "";
            
            this.init();
            
            return "deleteline.xhtml";
            
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
            return "deleteline.xhtml";
        }
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the lista
     */
    public List<BusLine> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<BusLine> lista) {
        this.lista = lista;
    }
    
}
