/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans;

import br.com.serverus.actions.DoNewPath;
import br.com.serverus.actions.DoGetAllPaths;
import br.com.serverus.actions.DoDeletePath;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.Path;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

/**
 * Gerencia as informações relacionadas as ruas
 *
 * @author eduardo-lima
 */
@ManagedBean
public class PathMB {

    private String nome;
    private String idStr;

    private List<Path> lista;

    @PostConstruct
    public void init() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

        DoGetAllPaths acao = new DoGetAllPaths();
        setLista(new ArrayList<>());

        try {
            acao.execute();

            getLista().addAll(acao.getList());
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }
    
    
    /**
     * Método para adicionar ruas base
     *
     * @param evt Evento de componente jsf
     */
    public void add(ActionEvent evt) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);
        
        DoNewPath acao = new DoNewPath(session.getAttribute("usuario").toString(), nome);
        
        try {
            acao.execute(); // Executa a ação
            this.addInfo("Registro adicionado");
            this.nome = "";
            
            this.init();
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    
    /**
     * Método para remover a rua selecionada
     *
     * @return Redirecionamento para outra página
     */
    public String delete() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);
        
        DoDeletePath acao = new DoDeletePath(session.getAttribute("usuario").toString(), this.idStr);
        
        try {
            acao.execute();
            this.addInfo("Registro removido");
            
            this.idStr = null;
            this.nome = "";
            
            this.init();
            
            return "deletepath.xhtml";
            
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
            return "deletepath.xhtml";
        }
    }
    
    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * informação
     *
     * @param info Informação a ser exibida
     */
    private void addInfo(String info) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, info, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * erro
     *
     * @param erro Erro a ser exibido
     */
    private void addError(String erro) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the lista
     */
    public List<Path> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<Path> lista) {
        this.lista = lista;
    }

    /**
     * @return the idStr
     */
    public String getIdStr() {
        return idStr;
    }

    /**
     * @param idStr the idStr to set
     */
    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

}
