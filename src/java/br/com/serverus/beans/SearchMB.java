/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans;

import br.com.serverus.actions.DoGetAllScheduleTypes;
import br.com.serverus.actions.DoGetLineCoordinates;
import br.com.serverus.actions.DoGetLineSchedules;
import br.com.serverus.actions.DoGetLinesByPath;
import br.com.serverus.beans.services.BusLineService;
import br.com.serverus.beans.services.PathService;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.Coordinates;
import br.com.serverus.models.BusLine;
import br.com.serverus.models.Path;
import br.com.serverus.models.ScheduleC;
import br.com.serverus.models.ScheduleType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.model.map.Circle;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.primefaces.model.map.Polyline;

/**
 * Gerencia todas as ações que envolvem a página de busca.
 *
 * @author eduardo-lima
 */
@ManagedBean
@ViewScoped
public class SearchMB implements Serializable {

    // Campo referente a rua escolhida
    private Path selectedPath;
    // Lista de ônibus encontrados
    private List<BusLine> busLineFilteredList;
    // Código do ônibus escolhidos
    private String selectedBusLine;
    // Código do horário
    private int selectedSchedule;
    // Flag de ida ou volta
    private int selectedRouteType;
    // Lista de horários do ônibus escolhido
    private List<ScheduleType> listScheduleTypes;
    // Tipo selecionado
    private int selectedScheduleType;
    // Lista de horários traduzida
    private List<ScheduleC> listSchedules;

    private MapModel model;

    // Objeto que obtém as ruas cadastradas
    @ManagedProperty("#{pathService}")
    private PathService service;

    /**
     * Método disparado conforme valor digitado
     *
     * @param qry Valor definido para busca
     * @return Lista de ruas
     */
    public List<Path> completeText(String qry) {

        // Obtém todas as ruas
        List<Path> listaRuas = service.getLista();
        List<Path> listaFiltrada = new ArrayList<Path>();

        // Itera todas as ruas e compara se é igual ao valor para busca
        for (int i = 0; i < listaRuas.size(); i++) {
            Path r = listaRuas.get(i);
            if (r.getDescription().toLowerCase().contains(qry)) {
                listaFiltrada.add(r);
            }
        }
        return listaFiltrada;
    }

    /**
     * Método para buscar os horários conforme o bus selecionado
     *
     * @param code
     */
    public void loadScheduleView(String code) {

        this.selectedBusLine = code;
        this.listSchedules = null;

        try {
            // Buscar os tipos de horários
            DoGetAllScheduleTypes action = new DoGetAllScheduleTypes();

            action.execute();

            this.listScheduleTypes = action.getList();
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método para ver os horários de ônibus
     *
     * @param e
     */
    public void viewSchedules(ActionEvent e) {

        try {
            // Buscar os horários
            DoGetLineSchedules action = new DoGetLineSchedules(this.selectedBusLine, this.selectedScheduleType, this.getSelectedRouteType());

            action.execute();

            List<ScheduleC> list = action.getList();

            this.listSchedules = new ArrayList<>();
            // Traduzir os horários para String
            for (ScheduleC s : list) {
                this.listSchedules.add(s);
            }

        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método que gera o modelo para desenhar no mapa
     *
     * @param scheduleID ID do horário
     */
    public void drawRoute(int scheduleID) {

        this.selectedSchedule = scheduleID;
        try {
            DoGetLineCoordinates acao = new DoGetLineCoordinates(this.selectedBusLine, this.selectedSchedule);
            acao.execute();

            List<Coordinates> coordenadas = acao.getList();

            model = new DefaultMapModel();
            Polyline polyline = new Polyline();
            polyline.setStrokeColor("red");
            polyline.setStrokeWeight(8);

            int i = 0;
            for (Coordinates c : coordenadas) {

                if (i == 0) {
                    Marker m = new Marker(new LatLng(c.getLatitude(), c.getLongitude()));
                    m.setIcon("http://maps.google.com/mapfiles/ms/micons/green-dot.png");
                    model.addOverlay(m);
                }
                if (i == coordenadas.size() - 1) {
                    Marker m = new Marker(new LatLng(c.getLatitude(), c.getLongitude()));
                    m.setIcon("http://maps.google.com/mapfiles/ms/micons/red-dot.png");
                    model.addOverlay(m);
                }

                polyline.getPaths().add(new LatLng(c.getLatitude(), c.getLongitude()));
                i++;
            }
            model.addOverlay(polyline);

        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    public void verifyMapModel() {

        if (this.model != null) {
            
        }
    }

    /**
     * Método para buscar ônibus conforme a rua escolhida
     *
     * @param e
     */
    public void viewBusLineByRoad(ActionEvent e) {

        this.setBusLineFilteredList(new ArrayList<>());

        // Ver maneira de agrupar os objetos depois!
        DoGetLinesByPath acao = new DoGetLinesByPath(getSelectedPath());
        try {
            acao.execute();

            this.getBusLineFilteredList().addAll(acao.getList());

            this.selectedPath = null;
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * informação
     *
     * @param info Informação a ser exibida
     */
    private void addInfo(String info) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, info, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * erro
     *
     * @param erro Erro a ser exibido
     */
    private void addError(String erro) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @param service the service to set
     */
    public void setService(BusLineService service) {
        this.setService(service);
    }

    /**
     * @return the selectedPath
     */
    public Path getSelectedPath() {
        return selectedPath;
    }

    /**
     * @param selectedPath the selectedPath to set
     */
    public void setSelectedPath(Path selectedPath) {
        this.selectedPath = selectedPath;
    }

    /**
     * @return the service
     */
    public PathService getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(PathService service) {
        this.service = service;
    }

    /**
     * @return the busLineFilteredList
     */
    public List<BusLine> getBusLineFilteredList() {
        return busLineFilteredList;
    }

    /**
     * @param busLineFilteredList the busLineFilteredList to set
     */
    public void setBusLineFilteredList(List<BusLine> busLineFilteredList) {
        this.busLineFilteredList = busLineFilteredList;
    }

    /**
     * @return the model
     */
    public MapModel getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(MapModel model) {
        this.model = model;
    }

    /**
     * @return the selectedBusLine
     */
    public String getSelectedBusLine() {
        return selectedBusLine;
    }

    /**
     * @param selectedBusLine the selectedBusLine to set
     */
    public void setSelectedBusLine(String selectedBusLine) {
        this.selectedBusLine = selectedBusLine;
    }

    /**
     * @return the listScheduleTypes
     */
    public List<ScheduleType> getListScheduleTypes() {
        return listScheduleTypes;
    }

    /**
     * @param listScheduleTypes the listScheduleTypes to set
     */
    public void setListScheduleTypes(List<ScheduleType> listScheduleTypes) {
        this.listScheduleTypes = listScheduleTypes;
    }

    /**
     * @return the selectedScheduleType
     */
    public int getSelectedScheduleType() {
        return selectedScheduleType;
    }

    /**
     * @param selectedScheduleType the selectedScheduleType to set
     */
    public void setSelectedScheduleType(int selectedScheduleType) {
        this.selectedScheduleType = selectedScheduleType;
    }

    /**
     * @return the selectedRouteType
     */
    public int getSelectedRouteType() {
        return selectedRouteType;
    }

    /**
     * @param selectedRouteType the selectedRouteType to set
     */
    public void setSelectedRouteType(int selectedRouteType) {
        this.selectedRouteType = selectedRouteType;
    }

    /**
     * @return the listSchedules
     */
    public List<ScheduleC> getListSchedules() {
        return listSchedules;
    }

    /**
     * @param listSchedules the listSchedules to set
     */
    public void setListSchedules(List<ScheduleC> listSchedules) {
        this.listSchedules = listSchedules;
    }

    /**
     * @return the selectedSchedule
     */
    public int getSelectedSchedule() {
        return selectedSchedule;
    }

    /**
     * @param selectedSchedule the selectedSchedule to set
     */
    public void setSelectedSchedule(int selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
    }

}
