/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans;

import br.com.serverus.actions.DoAuthentication;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.User;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * Controla e possui as rotinas que relaciona o usuário logado
 *
 * @author eduardo-lima
 */
@ManagedBean
public class UserMB implements Serializable {

    /**
     * Objeto de controle entre a view e o bean
     */
    private User usuario;

    @PostConstruct
    public void init() {
        usuario = new User();
    }

    /**
     * Método responsável pela autenticação do usuário
     *
     * @return Página de redirecionamento
     */
    public String auth() {
        FacesContext fc = FacesContext.getCurrentInstance();

        DoAuthentication autenticador = new DoAuthentication(this.usuario.getLogin(), this.usuario.getSenha());

        try {
            autenticador.execute();

            FacesMessage fm = new FacesMessage("Usuário: " + this.usuario.getLogin());
            fm.setSeverity(FacesMessage.SEVERITY_INFO);
            fc.addMessage(null, fm);

            ExternalContext ec = fc.getExternalContext();
            HttpSession session = (HttpSession) ec.getSession(false);
            session.setAttribute("usuario", this.usuario.getLogin());

            return "main.xhtml";
        } catch (ActionExecutionEx eae) {
            FacesMessage fm = new FacesMessage("Usuário e/ou senha inválidos");
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            fc.addMessage(null, fm);

            return "auth.xhtml";
        }
    }

    public String logOut() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);
        session.removeAttribute("usuario");
        
        return "index.xhtml";
    }

    /**
     * @return the usuario
     */
    public User getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }

}
