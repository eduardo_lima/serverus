/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans;

import br.com.serverus.actions.DoNewItinerary;
import br.com.serverus.actions.DoGetAllLines;
import br.com.serverus.actions.DoGetAllPaths;
import br.com.serverus.actions.DoDeleteLineItinerary;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.BusLine;
import br.com.serverus.models.Path;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

/**
 * Gerencia as informações necessárias para exibição em mapa, nome da rota e
 * itinerario
 *
 * @author eduardo-lima
 */
@ManagedBean
@ViewScoped
public class ItineraryMB implements Serializable {

    // Lista que contém todos os registros de ônibus cadastrados
    private List<BusLine> busLineList;
    // Lista que contém todas as ruas cadastradas
    private List<Path> pathList;
    private String[] selectedPaths;

    // Código da linha selecionada
    private String busLineCode;

    @PostConstruct
    public void init() {

        DoGetAllLines acao = new DoGetAllLines();
        setBusLineList(new ArrayList<>());

        try {
            acao.execute();

            getBusLineList().addAll(acao.getList());
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }

        DoGetAllPaths acao2 = new DoGetAllPaths();
        setPathList(new ArrayList<>());

        try {
            acao2.execute();

            getPathList().addAll(acao2.getList());
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método para salvar o percurso (para polyline no gmap), salvar as ruas
     * para pesquisa e o título da viagem
     *
     * @param evt Evento do componente
     */
    public void save(ActionEvent evt) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);

        try {
            this.cleanItinerary();

            // Inserir ruas do itinerário (para buscar os registros)
            DoNewItinerary acao2 = new DoNewItinerary(session.getAttribute("usuario").toString(), this.busLineCode, this.selectedPaths);
            acao2.execute();
            
            // Resetar a página
            this.busLineCode = null;
            this.selectedPaths = null;
            
            this.addInfo("Itinerário registrado!");
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método que reinicia com a nova configuração do itinerário
     */
    private void cleanItinerary() throws ActionExecutionEx {

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

        DoDeleteLineItinerary acao2 = new DoDeleteLineItinerary(session.getAttribute("usuario").toString(), this.busLineCode);
        acao2.execute();
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * informação
     *
     * @param info Informação a ser exibida
     */
    private void addInfo(String info) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, info, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * erro
     *
     * @param erro Erro a ser exibido
     */
    private void addError(String erro) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the busLineList
     */
    public List<BusLine> getBusLineList() {
        return busLineList;
    }

    /**
     * @param busLineList the busLineList to set
     */
    public void setBusLineList(List<BusLine> busLineList) {
        this.busLineList = busLineList;
    }

    /**
     * @return the busLineCode
     */
    public String getBusLineCode() {
        return busLineCode;
    }

    /**
     * @param busLineCode the busLineCode to set
     */
    public void setBusLineCode(String busLineCode) {
        this.busLineCode = busLineCode;
    }

    /**
     * @return the pathList
     */
    public List<Path> getPathList() {
        return pathList;
    }

    /**
     * @param pathList the pathList to set
     */
    public void setPathList(List<Path> pathList) {
        this.pathList = pathList;
    }

    /**
     * @return the selectedPaths
     */
    public String[] getSelectedPaths() {
        return selectedPaths;
    }

    /**
     * @param selectedPaths the selectedPaths to set
     */
    public void setSelectedPaths(String[] selectedPaths) {
        this.selectedPaths = selectedPaths;
    }
}
