/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans;

import br.com.serverus.actions.DoDeleteSchedule;
import br.com.serverus.actions.DoGetAllLines;
import br.com.serverus.actions.DoGetAllSchedules;
import br.com.serverus.actions.DoNewSchedule;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.BusLine;
import br.com.serverus.models.ScheduleC;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

/**
 * Gerenciador para a manipulação dos registros de horários
 * @author eduardo-lima
 */
@ManagedBean
@ViewScoped
public class ScheduleMB implements Serializable {

    // Código da linha selecionada
    private String codigoLinha;
    // Lista que contém todos os registros de ônibus cadastrados
    private List<BusLine> listaOnibus;
    // Flag que indica rota de ida ou volta
    private int idaVolta;
    // Flag que indica os tipos o tipo de horário
    private int tipoHorario;
    // Horário de saída do ônibus
    private Date saida;
    // Lista específica para consulta em tabela
    private List<ScheduleC> listAllSchedules;

    @PostConstruct
    public void init() {

        DoGetAllLines acao = new DoGetAllLines();
        DoGetAllSchedules action2 = new DoGetAllSchedules();

        try {
            acao.execute();
            this.setListaOnibus(acao.getList());

            action2.execute();
            this.setListAllSchedules(action2.getList());
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    public void save(ActionEvent e) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);

        try {
            if (this.saida == null) {
                throw new IllegalArgumentException("Não foi informado o horário de partida");
            }

            Calendar c = Calendar.getInstance();
            c.setTime(this.saida);

            DoNewSchedule action = new DoNewSchedule(session.getAttribute("usuario").toString(), this.codigoLinha, this.idaVolta, this.tipoHorario, (c.get(Calendar.HOUR_OF_DAY) * 60) + c.get(Calendar.MINUTE));
            action.execute();

            this.codigoLinha = null;
            this.idaVolta = 0;
            this.tipoHorario = 0;
            this.saida = null;
            
            this.init();
            
            this.addInfo("Horário adicionado");
        } catch (ActionExecutionEx | IllegalArgumentException ex) {
            this.addError(ex.getMessage());
        }
    }

    public void delete(int id) {

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);
        
        try {
            DoDeleteSchedule action = new DoDeleteSchedule(session.getAttribute("usuario").toString(), id);
            action.execute();
            
            this.addInfo("Horário removido");
            this.init();
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * informação
     *
     * @param info Informação a ser exibida
     */
    private void addInfo(String info) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, info, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * erro
     *
     * @param erro Erro a ser exibido
     */
    private void addError(String erro) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the listaOnibus
     */
    public List<BusLine> getListaOnibus() {
        return listaOnibus;
    }

    /**
     * @param listaOnibus the listaOnibus to set
     */
    public void setListaOnibus(List<BusLine> listaOnibus) {
        this.listaOnibus = listaOnibus;
    }

    /**
     * @return the idaVolta
     */
    public int getIdaVolta() {
        return idaVolta;
    }

    /**
     * @param idaVolta the idaVolta to set
     */
    public void setIdaVolta(int idaVolta) {
        this.idaVolta = idaVolta;
    }

    /**
     * @return the tipoHorario
     */
    public int getTipoHorario() {
        return tipoHorario;
    }

    /**
     * @param tipoHorario the tipoHorario to set
     */
    public void setTipoHorario(int tipoHorario) {
        this.tipoHorario = tipoHorario;
    }

    /**
     * @return the codigoLinha
     */
    public String getCodigoLinha() {
        return codigoLinha;
    }

    /**
     * @param codigoLinha the codigoLinha to set
     */
    public void setCodigoLinha(String codigoLinha) {
        this.codigoLinha = codigoLinha;
    }

    /**
     * @return the saida
     */
    public Date getSaida() {
        return saida;
    }

    /**
     * @param saida the saida to set
     */
    public void setSaida(Date saida) {
        this.saida = saida;
    }

    /**
     * @return the listAllSchedules
     */
    public List<ScheduleC> getListAllSchedules() {
        return listAllSchedules;
    }

    /**
     * @param listAllSchedules the listAllSchedules to set
     */
    public void setListAllSchedules(List<ScheduleC> listAllSchedules) {
        this.listAllSchedules = listAllSchedules;
    }

}
