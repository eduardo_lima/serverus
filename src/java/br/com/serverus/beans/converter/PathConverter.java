/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans.converter;

import br.com.serverus.beans.services.PathService;
import br.com.serverus.models.Path;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Conversor para objetos de ruas
 * @author eduardo-lima
 */
@FacesConverter("pathConverter")
public class PathConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                PathService service = (PathService) context.getExternalContext().getApplicationMap().get("pathService");
                
                List<Path> lista = service.getLista();
                
                for (Path r : lista){
                    if (value.equals(Integer.toString(r.getIdSQL()))){
                        return lista.get(lista.indexOf(r));
                    }
                }
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        } else {
            return null;
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null) {
            return String.valueOf(value);
        } else {
            return null;
        }

    }

}
