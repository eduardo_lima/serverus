/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans.services;

import br.com.serverus.actions.DoGetAllPaths;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.Path;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author home
 */
@ManagedBean(name = "pathService", eager = true)
@ApplicationScoped
public class PathService implements Serializable {

    private List<Path> lista;
    
    /**
     * @return the lista
     */
    public List<Path> getLista() {
        DoGetAllPaths acao = new DoGetAllPaths();
        List<Path> lista = new ArrayList<>();
        try {
            acao.execute();

            lista.addAll(acao.getList());
        } catch (ActionExecutionEx ex) {
            //Silenciada
        }
        return lista;
    }
    
}
