/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans.services;

import br.com.serverus.actions.DoGetAllLines;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.BusLine;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author home
 */
@ManagedBean(name = "busLineService", eager = true)
@ViewScoped
public class BusLineService {

    private List<BusLine> lista;

    @PostConstruct
    public void init() {

        DoGetAllLines acao = new DoGetAllLines();
        lista = new ArrayList<>();
        try {
            acao.execute();

            getLista().addAll(acao.getList());
        } catch (ActionExecutionEx ex) {
            //Silenciada
        }
    }

    /**
     * @return the lista
     */
    public List<BusLine> getLista() {
        return lista;
    }

}
