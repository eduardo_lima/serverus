/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.beans;

import br.com.serverus.actions.DoDeleteRide;
import br.com.serverus.actions.DoGetAllBusRides;
import br.com.serverus.actions.DoGetAllLines;
import br.com.serverus.actions.DoGetSchedulesBusLine;
import br.com.serverus.actions.DoNewRide;
import br.com.serverus.actions.DoNewRoute;
import br.com.serverus.exceptions.ActionExecutionEx;
import br.com.serverus.models.BusLine;
import br.com.serverus.models.BusRideC;
import br.com.serverus.models.ScheduleC;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpSession;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 * Gerenciador para manipular os registros de viagens
 *
 * @author eduardo-lima
 */
@ManagedBean
@ViewScoped
public class BusRideMB implements Serializable {

    // Linha do ônibus
    private String busLine;
    // Lista de ônibus
    private List<BusLine> listAllBus;
    // Flag de tipo de rota
    private int routeType;
    // Flag para o tipo de horário
    private int scheduleType;
    // Tempo de viagem
    private int rideTime;
    // Distância da viagem;
    private int rideDistance;
    // Lista de ruas
    private List<ScheduleC> listSchedules;
    // Ruas selecionadas
    private String[] selectedSchedules;
    // XML das rotas do mapa
    private UploadedFile file;

    // Lista de todas as viagens
    private List<BusRideC> listAllRide;

    @PostConstruct
    public void init() {

        try {
            DoGetAllLines actionBus = new DoGetAllLines();
            actionBus.execute();
            DoGetAllBusRides actionRide = new DoGetAllBusRides();
            actionRide.execute();

            this.setListBus(actionBus.getList());
            this.setListAllRide(actionRide.getList());
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    public void changeBusLine(ValueChangeEvent ev) {
        this.loadSchedules((String) ev.getNewValue(), this.routeType, this.scheduleType);
    }

    public void changeRouteType(ValueChangeEvent ev) {
        this.loadSchedules(this.busLine, (int) ev.getNewValue(), this.scheduleType);
    }

    public void changeScheduleType(ValueChangeEvent ev) {
        if (ev.getNewValue() != null) {
            this.loadSchedules(this.busLine, this.routeType, (int) ev.getNewValue());
        }
    }

    private void loadSchedules(String code, int route, int schedule) {
        try {
            DoGetSchedulesBusLine action = new DoGetSchedulesBusLine(code, route, schedule);
            action.execute();
            this.listSchedules = action.getList();

        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    public void save(ActionEvent e) {

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

        try {
            if (this.selectedSchedules.length <= 0) {
                throw new IllegalArgumentException("Não há horários marcados de saída");
            }

            for (String s : this.selectedSchedules) {
                DoNewRide action1 = new DoNewRide(session.getAttribute("usuario").toString(), busLine, Integer.parseInt(s), rideTime, rideDistance);
                action1.execute();
            }

            this.addInfo("Viagem adicionada!");
        } catch (ActionExecutionEx | IllegalArgumentException ex) {
            this.addError(ex.getMessage());
            return;
        }

        try {
            for (String s : this.selectedSchedules) {
                DoNewRoute action2 = new DoNewRoute(session.getAttribute("usuario").toString(), busLine, Integer.parseInt(s), extractFileUpload());
                action2.execute();
            }

            this.addInfo("Rota adicionada!");
        } catch (ActionExecutionEx | IllegalArgumentException ex) {
            this.addError("Rota não adicionada: " + ex.getMessage());
        } catch (IOException ex) {
            this.addError("Problemas para ler o arquivo KML");
        }
        this.busLine = null;
        this.routeType = 0;
        this.scheduleType = 0;
        this.rideDistance = 0;
        this.rideTime = 0;
        this.listSchedules = null;
        this.file = null;
    }

    public void delete(String busLineID, int busScheduleID) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(false);

        try {
            DoDeleteRide action = new DoDeleteRide(session.getAttribute("usuario").toString(), busLineID, busScheduleID);
            action.execute();

            this.init();
            
            this.addInfo("Viagem removida");
            this.init();
        } catch (ActionExecutionEx ex) {
            this.addError(ex.getMessage());
        }
    }

    /**
     * Método que lê e grava em arquivo temporário com base no upload do arquivo
     * realizado
     *
     * @return Instância do objeto File
     * @throws IOException Exceção de falha na leitura
     */
    private File extractFileUpload() throws IOException {

        if (this.getFile() != null) {
            // Definir local temporário
            final String caminhoTemp = "C:\\BusWorkspace\\temp\\";

            // Instanciar objeto de leitura
            BufferedReader lerArq = new BufferedReader(new InputStreamReader(this.getFile().getInputstream()));

            // Instaciar objetos de gravação
            FileWriter arq = new FileWriter(caminhoTemp + this.getFile().getFileName());
            PrintWriter gravarArq = new PrintWriter(arq);

            // Lé as linhas do arquivo
            String linhaArq = lerArq.readLine();
            // Iterar as linhas do arquivo
            while (linhaArq != null) {
                gravarArq.printf(linhaArq);

                linhaArq = lerArq.readLine();
            }
            arq.close();

            return new File(caminhoTemp + this.getFile().getFileName());
        }
        return null;
    }

    /**
     * Evento disparado assíncronamente ao carregar o arquivo kml
     *
     * @param event
     */
    public void uploadKML(FileUploadEvent event) {
        this.setFile(event.getFile());

        this.addInfo("Arquivo carregado");
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * informação
     *
     * @param info Informação a ser exibida
     */
    private void addInfo(String info) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, info, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Método para o managed bean lançar a mensagem para a página JSF do tipo
     * erro
     *
     * @param erro Erro a ser exibido
     */
    private void addError(String erro) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the busLine
     */
    public String getBusLine() {
        return busLine;
    }

    /**
     * @param busLine the busLine to set
     */
    public void setBusLine(String busLine) {
        this.busLine = busLine;
    }

    /**
     * @return the listAllBus
     */
    public List<BusLine> getListBus() {
        return listAllBus;
    }

    /**
     * @param listBus the listAllBus to set
     */
    public void setListBus(List<BusLine> listBus) {
        this.listAllBus = listBus;
    }

    /**
     * @return the rideTime
     */
    public int getRideTime() {
        return rideTime;
    }

    /**
     * @param rideTime the rideTime to set
     */
    public void setRideTime(int rideTime) {
        this.rideTime = rideTime;
    }

    /**
     * @return the rideDistance
     */
    public int getRideDistance() {
        return rideDistance;
    }

    /**
     * @param rideDistance the rideDistance to set
     */
    public void setRideDistance(int rideDistance) {
        this.rideDistance = rideDistance;
    }

    /**
     * @return the listSchedules
     */
    public List<ScheduleC> getListSchedules() {
        return listSchedules;
    }

    /**
     * @param listSchedules the listSchedules to set
     */
    public void setListSchedules(List<ScheduleC> listSchedules) {
        this.listSchedules = listSchedules;
    }

    /**
     * @return the selectedSchedules
     */
    public String[] getSelectedSchedules() {
        return selectedSchedules;
    }

    /**
     * @param selectedSchedules the selectedSchedules to set
     */
    public void setSelectedSchedules(String[] selectedSchedules) {
        this.selectedSchedules = selectedSchedules;
    }

    /**
     * @return the routeType
     */
    public int getRouteType() {
        return routeType;
    }

    /**
     * @param routeType the routeType to set
     */
    public void setRouteType(int routeType) {
        this.routeType = routeType;
    }

    /**
     * @return the scheduleType
     */
    public int getScheduleType() {
        return scheduleType;
    }

    /**
     * @param scheduleType the scheduleType to set
     */
    public void setScheduleType(int scheduleType) {
        this.scheduleType = scheduleType;
    }

    /**
     * @return the file
     */
    public UploadedFile getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(UploadedFile file) {
        this.file = file;
    }

    /**
     * @return the listAllRide
     */
    public List<BusRideC> getListAllRide() {
        return listAllRide;
    }

    /**
     * @param listAllRide the listAllRide to set
     */
    public void setListAllRide(List<BusRideC> listAllRide) {
        this.listAllRide = listAllRide;
    }

}
