/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.interfaces;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.models.ScheduleC;
import java.util.List;

/**
 * Interface específica para a busca de horários
 *
 * @author eduardo-lima
 */
public interface ViewScheduleInterface {

    public List<ScheduleC> getSchedulesByBusLine(String code, int type, int routeType) throws DatabaseEx;

}
