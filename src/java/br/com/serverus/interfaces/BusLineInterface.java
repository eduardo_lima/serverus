/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.interfaces;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.models.BusLine;
import br.com.serverus.models.Path;
import br.com.serverus.models.Schedule;
import java.util.List;

/**
 * Interface que contém métodos específicos para a obtenção de linhas de ônibus
 * @author eduardo-lima
 */
public interface BusLineInterface {
    
    public List<BusLine> getBusLineByRoad(Path r) throws DatabaseEx;
    
}
