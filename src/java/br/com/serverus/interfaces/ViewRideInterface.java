/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.interfaces;

import br.com.serverus.exceptions.DatabaseEx;

/**
 * Interface para visualização das viagens de ônibus
 * @author eduardo-lima
 */
public interface ViewRideInterface {
    
    public void delete(String busLineID, int scheduleID) throws DatabaseEx;
}
