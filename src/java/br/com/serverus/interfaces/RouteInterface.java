/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.interfaces;

import br.com.serverus.exceptions.DatabaseEx;
import br.com.serverus.models.Coordinates;
import java.util.List;

/**
 * Interface específica para a obtenção de rotas de ônibusS
 * @author eduardo-lima
 */
public interface RouteInterface {

    @Deprecated
    public List<Coordinates> getCoordinatesByBusLine(String codigo, int idaVolta) throws DatabaseEx;
    
    public List<Coordinates> getCoordinates(String busLineID, int scheduleID) throws DatabaseEx;
}
