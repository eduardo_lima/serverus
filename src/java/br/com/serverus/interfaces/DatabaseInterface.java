/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.serverus.interfaces;

import br.com.serverus.exceptions.DatabaseEx;
import java.util.List;

/**
 * Interface para questões de add, edit e delete registro de maneira geral
 *
 * @author eduardo-lima
 * @param <T> Tipo genérico do registro
 */
public interface DatabaseInterface <T>{
    
    
    public void add(T object) throws DatabaseEx;
    
    public void edit(T object);

    public void delete(String id) throws DatabaseEx;

    public T get(String codigo);

    public List<T> getAll() throws DatabaseEx;
}
